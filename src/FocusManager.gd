class_name FocusManger extends Node

var focused_interactable: ComponentInteractable
var player: Player

var _interactables: Array[ComponentInteractable]

func add_interactable(interactable_component: ComponentInteractable) -> void:
	_interactables.append(interactable_component)

	interactable_component.state_changed.connect(
		_on_interactable_state_changed.bind(interactable_component)
	)

	_interactables.sort_custom(func(a, b):
		return a.interactable_owner.global_position.x < b.interactable_owner.global_position.x
	)

	if _interactables.size() == 1:
		_handle_interactables_changed()


func remove_interactable(interactable_component: ComponentInteractable) -> void:
	#interactable_component.interactable_owner._sprite_2d.modulate.a = 1.0
	_interactables.erase(interactable_component)

	interactable_component.state_changed.disconnect(
		_on_interactable_state_changed
	)

	if focused_interactable == interactable_component:
		_handle_interactables_changed()


func focus_prev() -> void:
	_change_focus(-1)


func focus_next() -> void:
	_change_focus(1)


func _on_interactable_state_changed(state: bool, interactable_component: ComponentInteractable) -> void:
	if state or interactable_component != focused_interactable:
		return

	#interactable_component.interactable_owner._sprite_2d.modulate.a = 1.0
	_interactables.erase(interactable_component)

	_handle_interactables_changed()


func _handle_interactables_changed() -> void:
	var new_focused_interactable: ComponentInteractable

	if _interactables.size() == 1:
		new_focused_interactable = _interactables[0]
	else:
		new_focused_interactable = _find_closest_interactable()

	if focused_interactable != null:
		focused_interactable.has_focus = false

	if new_focused_interactable == null:
		SignalBus.ui_interaction_menu_hide_requested.emit()

		focused_interactable = null

		return

	focused_interactable = new_focused_interactable
	#_focused_interactable.interactable_owner._sprite_2d.modulate.a = .5

	SignalBus.ui_interaction_menu_show_requested.emit(focused_interactable)
	focused_interactable.has_focus = true


func _find_closest_interactable() -> ComponentInteractable:
	var closest_distance := -1.0
	var closest_interactable: ComponentInteractable
	var player_pos := player.global_position

	for interactable in _interactables:
		var distance_to_actor := player_pos.distance_to(
			interactable.interactable_owner.global_position
		)

		if closest_distance > distance_to_actor or closest_distance == -1:
			closest_distance = distance_to_actor
			closest_interactable = interactable

	return closest_interactable


func _change_focus(move_focus_by: int) -> void:
	if _interactables.size() <= 1:
		return

	#SignalBus.ui_interaction_menu_hide_requested.emit()
	focused_interactable.has_focus = false

	var current_index := _interactables.find(focused_interactable)
	var next_index := current_index + move_focus_by
	var num_interactables := _interactables.size()

	if next_index > num_interactables - 1:
		next_index = 0
	elif next_index < 0:
		next_index = num_interactables - 1

	#_focused_interactable.interactable_owner._sprite_2d.modulate.a = 1.0
	focused_interactable = _interactables[next_index]
	#_focused_interactable.interactable_owner._sprite_2d.modulate.a = .5

	SignalBus.ui_interaction_menu_show_requested.emit(focused_interactable)
	focused_interactable.has_focus = true
