class_name GameManager extends Node

@export var _player_scene: PackedScene

@onready var _focus_manager: FocusManger = $FocusManger
@onready var _ui: UiPlayer = $Ui
@onready var _interactions_ui: UiInteractionRequirements = $InteractionRequirementsUi
@onready var _map: Map = $DungeonTest

var _player: Player
var _player_data_snapshot: Dictionary[int, RPlayer]

func _ready() -> void:
#TODO tmp, replace with loaded data (persistence)
	var player_data = preload("res://src/data/player/player.tres")
	_player_data_snapshot[0] = player_data.clone()

	_player = _create_player(player_data)
	_finish_player_setup(_player)

#TODO tmp, map needs to be created form selection or loaded data
	#_map._focus_manager = _focus_manager
	_map.preparation_done.connect(func(): _map.spawn_player(_player))
	_map.checkpoint_activated.connect(_on_checkpoint_activated)


func _on_checkpoint_activated() -> void:
	_player_data_snapshot[0] = _player.data.clone()


func _on_player_died() -> void:
	_map.player_died()
	_player.queue_free()

	await get_tree().physics_frame

	_player = _create_player(_player_data_snapshot[0])
	_finish_player_setup(_player)

	_map.spawn_player(_player)


func _create_player(player_data: RPlayer) -> Player:
	var player := _player_scene.instantiate() as Player

	player.data = player_data
	player.runtime_stats.current_hp = player_data.base_stats.max_hp

	return player


func _finish_player_setup(player: Player) -> void:
	_focus_manager.player = player

	player.died.connect(_on_player_died)
	player.focus_manager = _focus_manager

	_ui.setup(player)
	_interactions_ui.setup(player.data, _focus_manager)
