extends Node

enum INPUT_METHOD{
	MOUSE_AND_KEYBOARD
}

enum INTERACTION_INPUTS{
	NONE,
	INTERACT,
	INVENTORY,
}

var interaction_inputs_map: Dictionary = {
	INTERACTION_INPUTS.NONE: "",
	INTERACTION_INPUTS.INTERACT: "interact",
	INTERACTION_INPUTS.INVENTORY: "inventory",
}

var _current_input_method: INPUT_METHOD

func get_action_name(index: INTERACTION_INPUTS) -> String:
	return interaction_inputs_map[index]


func get_input_for_action(action: String) -> String:
	for event in InputMap.action_get_events(action):
		if _current_input_method == INPUT_METHOD.MOUSE_AND_KEYBOARD and event is InputEventKey:
			var keycode = DisplayServer.keyboard_get_keycode_from_physical(
				event.physical_keycode
			)

			return OS.get_keycode_string(keycode)

	return "ERROR"


func get_key_for_interaction_input(input: INTERACTION_INPUTS) -> String:
	return get_input_for_action(get_action_name(input))


func get_physical_keycode(action: String) -> Key:
	for event in InputMap.action_get_events(action):
		if _current_input_method == INPUT_METHOD.MOUSE_AND_KEYBOARD and event is InputEventKey:
			return event.physical_keycode

	return KEY_NONE
