extends Node

func format_decimals(value: float, decimals: int) -> float:
	var power := pow(10, decimals)

	value = int(value * power)

	return value / power


func round_decimals(value: float, decimals: int) -> float:
	var power := pow(10, decimals)

	value = round(value * power) / power

	return value


##Splits a number into multiple different numbers which in sum are equal to the
##inital number
func distribut_number_in_uniform_parts(number: int, parts: int) -> Array:
	var numbers: Array[int] = [0]

	for i in parts - 1:
		numbers.append(randi_range(1, number))

	numbers.append(number)
	numbers.sort()

	return numbers
