extends Node

func get_script_property_list(resource: Resource) -> Array[ScriptProperty]:
	var properties: Array[ScriptProperty] = []

	for property in resource.get_property_list():
		var is_script_var: bool = property.usage & PROPERTY_USAGE_SCRIPT_VARIABLE

		if not is_script_var:
			continue

		if property.type == TYPE_DICTIONARY or property.type == TYPE_OBJECT:
			continue

		properties.append(
			ScriptProperty.new(property.name, property.type)
		)

	return properties


class ScriptProperty:
	var name: String
	var type: Variant.Type

	@warning_ignore("shadowed_variable")
	func _init(name: String, type: Variant.Type) -> void:
		self.name = name
		self.type = type
