extends Node

@warning_ignore_start("unused_signal")
#region enemies
signal enemy_died(entity: Enemy)
signal enemy_death_effect_requested(effect: String, pos: Vector2)
signal enemy_loot_dropped(drops: Array[RItemQuan], pos: Vector2)
#endregion

#region ui
signal ui_item_requirements_requested(
	requirement: Array[RItemRequirement],
	interactable_component: ComponentInteractable
)

signal ui_item_requirements_removed(
	interactable_component: ComponentInteractable
)

signal ui_add_element_requested(element: Control, track_in_ui_stack: bool)
signal ui_remove_element_requested(element: Control)
signal ui_inventory_item_consumed(item_quan: RItemQuan)
signal ui_inventory_item_equipped(item: RItemEquipment)
signal ui_inventory_item_unequipped(item: RItemEquipment)
signal ui_add_buff_requested(buff: Buff)
#endregion

#region interaction
signal ui_interaction_menu_show_requested(interactable: ComponentInteractable)
signal ui_interaction_menu_hide_requested()
#endregion

#region resource nodes
#signal resource_node_mouse_entered(node: ResourceNode)
#signal resource_node_mouse_exited()
#endregion

#region dungeon
signal dungeon_started(room_stats: RDungeonRuntimeData)
#endregion

##region player
#signal player_died(player: Player)
##endregion

signal create_loot_requested(loot: Array[RLoot], crate_pos: Vector2)
