extends Node

@onready var _audio_stream_player: AudioStreamPlayer = $AudioStreamPlayer


func play_pickup_sound(audio_stream: AudioStream) -> void:
	_audio_stream_player.stream = audio_stream
	_audio_stream_player.play()


func play_death_sound(audio_stream: AudioStream) -> void:
	_audio_stream_player.stream = audio_stream
	_audio_stream_player.play()


func play_item_consume_sound(audio_stream: AudioStream) -> void:
	_audio_stream_player.stream = audio_stream
	_audio_stream_player.play()


func play_equipment_sound(audio_stream: AudioStream) -> void:
	_audio_stream_player.stream = audio_stream
	_audio_stream_player.play()
