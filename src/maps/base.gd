class_name Base extends Map

func _ready() -> void:
	_player_spawn = $PlayerSpawn

	await get_tree().physics_frame

	preparation_done.emit()

	super._ready()
