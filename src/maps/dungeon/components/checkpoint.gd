class_name ComponentDungeonCheckpoint extends Node2D

signal aktivated()

@onready var player_spawn: Marker2D = $PlayerSpawn

func _on_player_detector_body_entered(_body: Node2D) -> void:
	aktivated.emit()
