@tool
class_name ComponentSpawnArea extends Polygon2D

var _spawnable_zones: Array[TriangleArea]

@export var _entity_container: Node2D
@export var _show_polygon_triangles: bool:
	set(value):
		_show_polygon_triangles = value
		queue_redraw()

func _ready() -> void:
	var triangles := []
	var array := []

	for triangle in Geometry2D.triangulate_polygon(polygon):
		array.append(polygon[triangle])

		if array.size() == 3:
			triangles.append(array)
			array = []

	var triangles_with_proportion: Array[TriangleArea] = []
	var sum_of_areas = 0.0

	for triangle in triangles:
		var area = _calc_area_of_triangle(triangle)
		sum_of_areas += area
		triangles_with_proportion.append(
			TriangleArea.new(triangle, area)
		)

	var prev_proportion = 0.0

	for triangle in triangles_with_proportion:
		triangle.proportion = triangle.area / sum_of_areas * 100

		var next_proportion = prev_proportion + triangle.proportion
		triangle.rand_pick_range = Vector2(prev_proportion, next_proportion)

		prev_proportion = next_proportion

	_spawnable_zones = triangles_with_proportion


func _draw() -> void:
	if !_show_polygon_triangles:
		return

	for triangle in _spawnable_zones:
		draw_colored_polygon(
			triangle.triangle, Color(randf(), randf(), randf(), .5)
		)


func _calc_area_of_triangle(triangle: Array) -> float:
	return (triangle[0].x * triangle[1].y + triangle[1].x * triangle[2].y + triangle[2].x * triangle[0].y - \
		triangle[0].x * triangle[2].y - triangle[1].x * triangle[0].y - triangle[2].x * triangle[1].y) / 2


func _calc_x_pos(rand_pos_uniform: Vector2, triangular_zone: Array) -> float:
	return (1 - sqrt(rand_pos_uniform.x)) * triangular_zone[0].x + \
		(sqrt(rand_pos_uniform.x) * (1 - rand_pos_uniform.y)) * triangular_zone[1].x + \
		(sqrt(rand_pos_uniform.x) * rand_pos_uniform.y) * triangular_zone[2].x


func _calc_y_pos(rand_pos_uniform: Vector2, triangular_zone: Array) -> float:
	return (1 - sqrt(rand_pos_uniform.x)) * triangular_zone[0].y + \
		(sqrt(rand_pos_uniform.x) * (1 - rand_pos_uniform.y)) * triangular_zone[1].y + \
		(sqrt(rand_pos_uniform.x) * rand_pos_uniform.y) * triangular_zone[2].y


func _calc_rand_spawn_position() -> Vector2:
	var rand_num = randi_range(0, 100)
	var rand_zone: TriangleArea

	for zone in _spawnable_zones:
		if zone.rand_pick_range.x <= rand_num and zone.rand_pick_range.y >= rand_num:
			rand_zone = zone
			break

	var rand_pos_uniform := Vector2(randf(), randf())

	return Vector2(
		_calc_x_pos(rand_pos_uniform, rand_zone.triangle),
		_calc_y_pos(rand_pos_uniform, rand_zone.triangle)
	)


func spawn(entity: CollisionObject2D) -> void:
	entity.position = _calc_rand_spawn_position()

	_entity_container.add_child(entity)


class TriangleArea:
	var triangle: PackedVector2Array
	var area: float
	var proportion: float
	var rand_pick_range: Vector2

	@warning_ignore("shadowed_variable")
	func _init(triangle: PackedVector2Array, area: float) -> void:
		self.triangle = triangle
		self.area = area
