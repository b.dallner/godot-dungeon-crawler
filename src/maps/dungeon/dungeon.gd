class_name Dungeon extends Map

signal _entrance_done()
signal _rooms_done()
signal _exit_done()

#TODO needs to be read from previous dungeon data or from UI input/selection
@export var _num_possible_rooms: Vector2i

#var _dungeon_runtime_data := RDungeonRuntimeData.new()
var _dungeon_runtime_data := preload("res://src/data/dungeon/dungeon_test.tres")

var _rng = RandomNumberGenerator.new()

var _entrance: DungeonEntrance
var _rooms: Array[DungeonRoom]
var _active_room: DungeonRoom

func _ready() -> void:
	if _dungeon_runtime_data == null:
		_rng.randomize()
		_rng.state = 0

		_dungeon_runtime_data = RDungeonRuntimeData.new()
#TODO let the user enter a seed?
		_dungeon_runtime_data.seed = hash(_rng.rng_seed)
#TODO check if we need to remember the state
		_dungeon_runtime_data.rng_state = _rng.state
	else:
		_rng.seed = _dungeon_runtime_data.rng_seed
		_rng.state = _dungeon_runtime_data.rng_state

	_prepare_map()

	SignalBus.dungeon_started.emit(_dungeon_runtime_data)

	super._ready()


func _physics_process(delta: float) -> void:
	if _dungeon_runtime_data.active_room_runtime_data.started:
		_dungeon_runtime_data.timer += delta
		_dungeon_runtime_data.active_room_runtime_data.timer += delta


func _on_player_left() -> void:
#TODO let the gamemanager change the map
	get_tree().change_scene_to_file.call_deferred("res://src/maps/Base.tscn")


func _on_room_started(room: DungeonRoom) -> void:
	_active_room = room
	_dungeon_runtime_data.active_room_runtime_data = room.room_runtime_data


func player_died() -> void:
	_active_room.reset()
	_dungeon_runtime_data.active_room_runtime_data = _active_room.room_runtime_data


func _prepare_map() -> void:
	_prepare_entrance()

	await _entrance_done
	_prepare_rooms()

	await _rooms_done
	_prepare_exit()

	await _exit_done
	preparation_done.emit()


func _prepare_entrance() -> void:
	_entrance = _dungeon_runtime_data.biome.room_entrance.instantiate()
	_active_room = _entrance

	_player_spawn = _entrance.player_spawn

	$Rooms.add_child.call_deferred(_entrance)

	await _entrance.ready
	_entrance_done.emit()


func _prepare_exit() -> void:
	var last_room := _rooms[_rooms.size() - 1]
	var free_connections = last_room.get_free_connections()
	var connection = free_connections[
		_rng.randi_range(0, free_connections.size() - 1)
	]

	var exit: DungeonExit = _dungeon_runtime_data.biome.room_exit.instantiate()
	var exit_connection_left = exit.get_free_connections()[DungeonRoomConnection.DIRECTIONS.LEFT]

	exit.player_left.connect(_on_player_left)
	exit.global_position.x = connection.global_position.x + \
		abs(exit_connection_left.global_position.x)

	$Rooms.add_child.call_deferred(exit)

	await exit.ready
	_exit_done.emit()


func _prepare_rooms() -> void:
	var num_rooms = _rng.randi_range(
		_num_possible_rooms.x, _num_possible_rooms.y
	)

	for i in num_rooms:
		var room_parameters = _dungeon_runtime_data.parameter

		var room: DungeonRoomCombat = _dungeon_runtime_data.biome.rooms.pick_random().instantiate()
		room.set_resource_manager(DungeonRoomBudgetManager.new(
			room_parameters, _dungeon_runtime_data.biome.enemies
		))

		_rooms.append(room)
		$Rooms.add_child.call_deferred(room)

		room.started.connect(_on_room_started)
		room.checkpoint_activated.connect(_on_checkpoint_activated)

		await room.ready

		var prev_room: DungeonRoom

		if i == 0:
			prev_room = _entrance
		else:
			prev_room = _rooms[i - 1]

		var free_connections := prev_room.get_free_connections()
		var selected_connection_prev_room = free_connections[
			_rng.randi_range(0, free_connections.size() - 1)
		]

		free_connections = room.get_free_connections().filter(func(c: DungeonRoomConnection):
			return c.location != selected_connection_prev_room.location
		)

		var selected_connection_room = free_connections[
			_rng.randi_range(0, free_connections.size() - 1)
		]

		if selected_connection_prev_room.global_position.x > 0:
			room.global_position.x = \
				abs(selected_connection_prev_room.global_position.x) + \
				abs(selected_connection_room.global_position.x)
		else:
			room.global_position.x = \
				selected_connection_prev_room.global_position.x + \
				0 - abs(selected_connection_room.global_position.x)

		selected_connection_prev_room.is_free = false
		selected_connection_room.is_free = false

	_rooms.sort_custom(func(a: DungeonRoom, b: DungeonRoom):
		return a.global_position < b.global_position
	)

	_rooms_done.emit()
