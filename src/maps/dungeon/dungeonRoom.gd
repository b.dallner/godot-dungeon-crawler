class_name DungeonRoom extends Node2D

signal started(room: DungeonRoom)

##The tilemap used by the current scene
##Has to be an export, becaus its needed before ready
@export var _tile_map: TileMapLayer
@export var _tile_map_entrance: TileMapLayer
@export var _tile_map_exit: TileMapLayer
@export var _connections: Array[DungeonRoomConnection]

var room_runtime_data := RDungeonRoomRuntimeData.new()

var _budget_manager: DungeonRoomBudgetManager

func set_resource_manager(resource_manager: DungeonRoomBudgetManager) -> void:
	_budget_manager = resource_manager


func get_size() -> Vector2:
	return _tile_map.get_used_rect().size * _tile_map.tile_set.tile_size


func get_free_connections() -> Array[DungeonRoomConnection]:
	return _connections.filter(func(c: DungeonRoomConnection):
		return c.is_free
	)


func _on_entrance_player_entered(body: Node2D) -> void:
	if body is not Player:
		return

	room_runtime_data.started = true

	started.emit(self)


func _on_entrance_player_exited(body: Node2D) -> void:
	if body is not Player:
		return

#TODO not save, this could trigger when the player walks back
	_tile_map_entrance.enabled = true


func _on_exit_player_entered(body: Node2D) -> void:
	if body is not Player:
		return

	_tile_map_exit.enabled = true


func reset() -> void:
	room_runtime_data = RDungeonRoomRuntimeData.new()
