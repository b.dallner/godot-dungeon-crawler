class_name DungeonRoomBudgetManager extends Node2D

signal tick(enemies: Array[Enemy])
signal finished()

##State of a dungeon room. As soon as the room is started, monster will spawm
##and the timer will start running
var _started: bool = false
var _budget_depleted: bool = false
var _current_num_enemies: int
var _budget: int

var _tick_timer: float = 0.0
var _ticks: int = 0
var _parameter: RDungeonRoomParameter
var _enemies: Array[REnemy]


func _init(parameter: RDungeonRoomParameter, enemies: Array[REnemy]) -> void:
	_parameter = parameter
	_enemies = enemies

	_budget = parameter.available_budget


func _physics_process(delta: float) -> void:
	if _started:
		_tick_timer += delta

		if is_equal_approx(_tick_timer, _parameter.tick_speed):
			_tick_timer = 0.0
			_tick()


func _on_enemy_died() -> void:
	_current_num_enemies -= 1

	if _current_num_enemies == 0 and _budget_depleted:
		finished.emit()


func _tick() -> void:
	var scaling = _parameter.scaling.sample(
		min(_ticks, _parameter.scaling.max_domain)
	)
	var scaled_num_enemies := int(_parameter.max_num_enemies *\
		(1 + scaling * _parameter.num_enemies_scaling_multiplier)
	)

	if _current_num_enemies >= _parameter.max_num_enemies:
		return

	var num_enemies_to_spawn := scaled_num_enemies - _current_num_enemies

	var scaled_tick_budget := int(_parameter.tick_budget *\
		(1 + scaling * _parameter.tick_budget_scaling_multiplier))

	#print("scaling: %.3f, budget: %.2f, enemies: %d, enemies to spawn %d" %
		#[scaling, scaled_tick_budget, scaled_num_enemies, num_enemies_to_spawn] )

	tick.emit(_choose_enemies(scaled_tick_budget, num_enemies_to_spawn))

	_ticks += 1


func _choose_enemies(tick_budget: int, num_enemies: int) -> Array[Enemy]:
	var budget = clampi(tick_budget, 0, _budget)

	var available_enemies := _enemies.filter(func(e: REnemy):
		return e.budget_costs <= budget
	)

	if available_enemies.size() == 0:
		_budget_depleted = true
		stop()

		return []

	var enemies: Array[Enemy]

	for i in num_enemies:
		var enemy_data := available_enemies.pick_random() as REnemy

		if enemy_data.budget_costs > budget:
			continue

		_budget -= enemy_data.budget_costs

		var enemy = Enemy.create(enemy_data)
		enemy.died.connect(_on_enemy_died)

		enemies.append(enemy)

		_current_num_enemies += 1

	return enemies


func start() -> void:
	_started = true


func stop() -> void:
	_started = false


func reset() -> void:
	_started = false
	_tick_timer = 0.0
	_ticks = 0
