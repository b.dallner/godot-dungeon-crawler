class_name DungeonRoomConnection extends Marker2D

enum DIRECTIONS{
	LEFT,
	RIGHT,
}

var is_free: bool = true
@export_flags("left", "right") var location
