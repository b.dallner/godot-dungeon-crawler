class_name DungeonRoomCombat extends DungeonRoom

signal checkpoint_activated()

@export var _checkpoint: ComponentDungeonCheckpoint

@onready var _component_spawn_area: ComponentSpawnArea = $SpawnAreaComponent

func _ready() -> void:
	_budget_manager.tick.connect(_on_budget_manager_tick)
	_budget_manager.finished.connect(_on_budget_manager_finished)

	add_child(_budget_manager)


func _on_checkpoint_aktivated() -> void:
	_tile_map_entrance.enabled = false
	checkpoint_activated.emit(_checkpoint)


func _on_entrance_player_entered(body: Node2D) -> void:
	if body is not Player:
		return

	super._on_entrance_player_entered(body)

	_budget_manager.start()


func _on_budget_manager_tick(enemies: Array) -> void:
		for enemy in enemies:
			_component_spawn_area.spawn(enemy)


func _on_budget_manager_finished() -> void:
	room_runtime_data.started = false

	_tile_map_exit.enabled = false


func reset() -> void:
	_budget_manager.reset()

	super.reset()
