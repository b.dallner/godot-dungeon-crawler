class_name DungeonExit extends DungeonRoom

signal player_left()

func _on_exit_body_entered(body: Node2D) -> void:
	if body is Player:
		player_left.emit()
