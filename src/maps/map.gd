class_name Map extends Node2D

@warning_ignore("unused_signal")
signal preparation_done()
signal checkpoint_activated()

const LOOT_SPREAD_ON_DROP = Vector2(-20,20)
const DEATH_SPRITE_FRAMES = preload("res://src/model/death_sprite_frames.tres")

#var _focus_manager: FocusManger
var _player: Player
var _player_spawn: Marker2D
var _active_checkpoint: ComponentDungeonCheckpoint

@onready var _items_container: Node2D = $ItemsContainer
@onready var _player_container: Node2D = $PlayerContainer

func _ready() -> void:
	SignalBus.enemy_died.connect(_on_enemy_death)
	SignalBus.enemy_death_effect_requested.connect(_on_death_effect_requested)
	SignalBus.enemy_loot_dropped.connect(_roll_loot)
	SignalBus.create_loot_requested.connect(_roll_loot)


func _on_enemy_death(_entity: Enemy) -> void:
#TODO where do we reward the player with exp?
#Or do we spawn the exp as orbs as well?
	pass


func _on_death_effect_requested(effect: String, effect_position: Vector2) -> void:
	var death_effect = AnimatedSprite2D.new()
	death_effect.sprite_frames = DEATH_SPRITE_FRAMES
	death_effect.global_position = effect_position

	add_child(death_effect)
	death_effect.animation_finished.connect(death_effect.queue_free)
	death_effect.play(effect)


func _on_checkpoint_activated(checkpoint: ComponentDungeonCheckpoint) -> void:
	_active_checkpoint = checkpoint
	_player_spawn = checkpoint.player_spawn

	checkpoint_activated.emit()


func spawn_player(player: Player) -> void:
	_player = player

	_player.global_position = _player_spawn.global_position

	_player_container.add_child(_player)


##Overwrite function
func player_died() -> void:
	pass


func _roll_loot(loot: Array[RLoot], drop_pos: Vector2) -> void:
	for l:RLoot in loot:
		var quan := randi_range(l.quantity_range.x, l.quantity_range.y)

		var numbers := Math.distribut_number_in_uniform_parts(
			randi_range(l.currency_value_range.x, l.currency_value_range.y),
			quan
		)

		for i in quan:
			#TODO add bezier curve loot spread
			var pos = Vector2(
				randf_range(LOOT_SPREAD_ON_DROP.x, LOOT_SPREAD_ON_DROP.y),
				randf_range(LOOT_SPREAD_ON_DROP.x, LOOT_SPREAD_ON_DROP.y)
			)

			var item := Item.create(
				l.item, numbers[i + 1] - numbers[i], l.pickup_delay_sec
			)

			item.global_position = drop_pos + pos

			_items_container.add_child.call_deferred(item)
