#TODO Enemies boosting each others speed when clumped together
class_name Enemy extends Actor

var data: REnemy

@export var _aggro_component: ComponentAggro

@onready var _state_machine: StateMachine = $StateMachine

static func create(enemy_data: REnemy) -> Enemy:
	var enemy := load(enemy_data.scene).instantiate() as Enemy

	enemy.data = enemy_data
	enemy.runtime_stats.current_hp = enemy_data.base_stats.max_hp

	return enemy


func _ready() -> void:
	var player_nodes = get_tree().get_nodes_in_group("Player")

	var players: Array[Node2D]
	for p in player_nodes:
		players.append(p)

	_state_machine.init()
	_aggro_component.add_targets(players)


func get_death_sound_effect() -> AudioStream:
	return data.death_sound_effect
