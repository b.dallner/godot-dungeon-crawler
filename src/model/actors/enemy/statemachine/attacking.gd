class_name StateEnemyAttacking extends State

@export var _state_machin_owner: CharacterBody2D
@export var _aggro_component: ComponentAggro

var _target: Node2D
var _attack_timer: Timer

func _on_target_changed(new_target: Node2D) -> void:
	_target = new_target

	if not _check_in_attack_range():
		transitioned.emit(self, StateMachine.STATES.CHASING)


func init(stats: RRuntimeStats, input_component: ComponentInput, state_machine_owner: CharacterBody2D) -> void:
	super.init(stats, input_component,state_machine_owner)

	_attack_timer = Timer.new()
	_attack_timer.one_shot = true
	_attack_timer.wait_time = _runtime_stats.attack_cooldown
	_attack_timer.timeout.connect(_attack)

	add_child(_attack_timer)


func enter() -> void:
	print("enemy attacking state entered")
	_target = _aggro_component.get_current_target()

	_attack()

	_aggro_component.target_changed.connect(_on_target_changed)


func exit() -> void:
	_aggro_component.target_changed.disconnect(_on_target_changed)



func physics_process(_delta: float) -> void:
	if is_zero_approx(_runtime_stats.current_hp) or _runtime_stats.current_hp < 0.0:
		transitioned.emit(self, StateMachine.STATES.DEATH)

		return

	if not _check_in_attack_range():
		transitioned.emit(self, StateMachine.STATES.CHASING)



func _check_in_attack_range() -> bool:
	var distance = _target.global_position.distance_to(
		_state_machin_owner.global_position
	)

	return distance <= _runtime_stats.attack_range


func _attack() -> void:
#TODO HIGH No idea how to do this
	print("enemy attack target " + _target.name)

	_attack_timer.start()
