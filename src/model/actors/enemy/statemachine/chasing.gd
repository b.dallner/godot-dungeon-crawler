class_name StateChasing extends State

@export var _state_machin_owner: CharacterBody2D
@export var _aggro_component: ComponentAggro

var _target: Node2D

func _on_target_changed(new_target: Node2D) -> void:
	_target = new_target


func enter() -> void:
	print("enemy chasing state entered")
	_target = _aggro_component.get_current_target()

	_aggro_component.target_changed.connect(_on_target_changed)


func exit() -> void:
	_aggro_component.target_changed.disconnect(_on_target_changed)


func physics_process(delta: float) -> void:
	if is_zero_approx(_runtime_stats.current_hp) or _runtime_stats.current_hp < 0.0:
		transitioned.emit(self, StateMachine.STATES.DEATH)

		return

	var direction = _state_machin_owner.global_position.direction_to(
		_target.global_position
	)

	_state_machin_owner.velocity = direction * _runtime_stats.sprinting_speed * delta

	var distance = _target.global_position.distance_to(
		_state_machin_owner.global_position
	)

	if distance <= _runtime_stats.attack_range:
		transitioned.emit(self, StateMachine.STATES.ATTACKING)

	_state_machin_owner.move_and_slide()
