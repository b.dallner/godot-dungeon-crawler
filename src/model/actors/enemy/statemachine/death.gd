class_name StateEnemyDeath extends State

@export var _state_machin_owner: Actor
@export var _loot_component: ComponentLoot

func enter() -> void:
	print("enemy death state entered")
	_state_machin_owner.died.emit()

	#SignalBus.enemy_died.emit(_state_machin_owner)
	#SignalBus.enemy_loot_dropped.emit(
		#_loot_component.get_loot(),
		#_state_machin_owner.global_position
	#)

	SoundBus.play_death_sound(
		_state_machin_owner.get_death_sound_effect()
	)

	_state_machin_owner.queue_free()
