class_name StateSpawning extends State

@export var _stats_component: ComponentStats

func enter() -> void:
	_stats_component.get_stats().is_invincible = true
#TODO play spawn animation and await spawn animation to be finished
	await get_tree().create_timer(1.0).timeout
	transitioned.emit(self, StateMachine.STATES.CHASING)


func exit() -> void:
	_stats_component.get_stats().is_invincible = false
