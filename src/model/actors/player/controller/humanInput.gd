class_name HumanInput extends MyInput

func process() -> void:
	direction = Input.get_vector(
		"move_left", "move_right", "move_forward", "move_back"
	)

	attack = Input.is_action_pressed("left_click")
	suicide = Input.is_action_just_pressed("suicide")
	interact = Input.is_action_just_pressed("interact")
