class_name Player extends Actor

var data: RPlayer
var focus_manager: FocusManger

@onready var _state_machine: StateMachine = $StateMachine
@onready var _stats_component: ComponentStats = $StatsComponent
@onready var _inventory_component: ComponentInventory = $InventoryComponent
@onready var _equipment_component: ComponentEquipment = $EquipmentComponent
@onready var _buffs: Node = $Buffs

func _ready() -> void:
	_inventory_component.inventory = data.inventory
	_equipment_component.equipment = data.equipment

	_state_machine.init()

	SignalBus.ui_inventory_item_consumed.connect(_on_item_consumed)


func _physics_process(_delta: float) -> void:
	data.player_pos = global_position


func _on_item_consumed(item_quan: RItemQuan) -> void:
	var item: RItemConsumeable = item_quan.item
	var is_item_consumed := 0
	var sorted: Array[Resource]

	sorted.append_array(item.stats_effects)
	sorted.append_array(item.stats_buffs)
	sorted.sort_custom(func(a, b): return a.priority < b.priority )

	for stats_effect in sorted:
		if stats_effect is RStatsEffect:
			is_item_consumed += int(
				_stats_component.apply_stats_effect(stats_effect)
			)
		elif stats_effect is RStatsBuff:
			if _stats_component.apply_stats_buff(stats_effect):
				_create_buff(stats_effect)

			is_item_consumed = true

	SoundBus.play_item_consume_sound(item.consumtion_sound)

	if is_item_consumed == 0:
		return

	_inventory_component.remove_item_quan(item_quan)


func _on_interactable_in_range(interactable: ComponentInteractable) -> void:
	focus_manager.add_interactable(interactable)


func _on_interactable_out_of_range(interactable: ComponentInteractable) -> void:
	focus_manager.remove_interactable(interactable)


func _create_buff(stats_buff: RStatsBuff) -> void:
	var buff := Buff.create(stats_buff)

	buff.buff_ticked.connect(_stats_component.apply_stats_effect)
	buff.buff_timeout.connect(func(b: Buff):
		_stats_component.remove_stats_buff(b.stats_buff)
	)

	_buffs.add_child(buff)

	SignalBus.ui_add_buff_requested.emit(buff)


func add_buff(stats_buff: RStatsBuff) -> void:
	if _stats_component.apply_stats_buff(stats_buff):
		_create_buff(stats_buff)


func remove_buff(stats_buff: RStatsBuff) -> void:
	var buffs: = _buffs.get_children().filter(func(c: Buff):
		return c.stats_buff == stats_buff
	)

	if buffs.size() == 0:
		return

	buffs[0].buff_timeout.emit(buffs[0])
	buffs[0].queue_free()


func check_required_items(items: Array[RItemRequirement]) -> bool:
	return items.all(func(item: RItemRequirement):
		return _inventory_component.has_item_quan(item.item_quan)
	)


func remove_items(item_quans: Array[RItemQuan]) -> void:
	for item_quan in item_quans:
		if item_quan.item is RItemKey:
			_inventory_component.remove_key(item_quan)
		elif item_quan.item is RItemCurrency:
			_inventory_component.change_currency(-item_quan.quantity)
		else:
			_inventory_component.remove_item_quan(item_quan)
