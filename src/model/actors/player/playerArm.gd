class_name PlayerArm extends Node2D

var _current_weapon: Weapon
var _tools: Array[Weapon]

func get_current_weapon() -> Weapon:
	return _current_weapon


func get_required_tool(required_tool_type: RItem.TOOL_TYPES) -> Weapon:
	for tool in _tools:
		if tool.get_tool_type() == required_tool_type:
			return tool

	return null


func equip_weapon(weapon: RItemWeapon) -> void:
	unequip_weapon()

	_current_weapon = load(weapon.model_scene_path).instantiate()
	_current_weapon.possessor = get_parent()

	add_child(_current_weapon)


func equip_tool(tool: RItemWeapon) -> void:
	unequip_tool(tool)

	var tool_node := load(tool.model_scene_path).instantiate() as Weapon

	tool_node.visible = false
	tool_node.possessor = get_parent()

	_tools.append(tool_node)
	add_child(tool_node)


func unequip_weapon() -> void:
	if _current_weapon != null:
		_current_weapon.queue_free()


func unequip_tool(tool: RItemWeapon) -> void:
	for t in _tools:
		if t._data == tool:
			t.queue_free()
			_tools.erase(t)

			return


func show_tool(tool: Weapon) -> void:
	if _current_weapon != null:
		_current_weapon.visible = false

	tool.visible = true


func show_weapon() -> void:
	for tool: Weapon in _tools:
		tool.visible = false

	if _current_weapon != null:
		_current_weapon.visible = true
