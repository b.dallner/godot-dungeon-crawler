class_name StateIdle extends PlayerState

@export var _hurtbox_component: ComponentWeaponHurtbox
#@export var _interactor_component: ComponentInteractor

func _on_hurtbox_hurt() -> void:
	transitioned.emit(self, StateMachine.STATES.COMBAT)


func _on_resource_node_clicked() -> void:
	transitioned.emit(self, StateMachine.STATES.GATHERING)


func enter() -> void:
	print("player idle state entered")

	_hurtbox_component.hurt.connect(_on_hurtbox_hurt)

#TODO interaction rework, check if needed
	#_interactor_component.resource_node_clicked.connect(_on_resource_node_clicked)


func exit() -> void:
	_hurtbox_component.hurt.disconnect(_on_hurtbox_hurt)

#TODO interaction rework, check if needed
	#_interactor_component.resource_node_clicked.disconnect(_on_resource_node_clicked)


func physics_process(delta: float) -> void:
	super.physics_process(delta)

	var next_state: StateMachine.STATES

	if _input_component.get_active_input().attack:
		next_state = StateMachine.STATES.COMBAT
	elif _input_component.get_active_input().direction != Vector2.ZERO:
		next_state = StateMachine.STATES.SPRINTING

	_rotate_arm(delta)

	if next_state:
		transitioned.emit(self, next_state)
