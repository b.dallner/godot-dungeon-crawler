class_name StatePlayerCombat extends PlayerState

@export_range(0, 5, 1, "or_greater","suffix:sec") var _time_remaining_in_combat_sec: int

var _weapon: Weapon

@onready var _timer_remain_in_combat: Timer = $TimerRemainInCombat

func _on_timer_remain_in_combat_timeout() -> void:
	transitioned.emit(self, StateMachine.STATES.SPRINTING)


func check_requirements() -> bool:
	return _arm.get_current_weapon() != null


func enter() -> void:
	print("player combat state entered")
	_weapon = _arm.get_current_weapon()

	if _weapon.is_attacking:
		return

	_weapon.attack()


func physics_process(delta: float) -> void:
	super.physics_process(delta)

	var speed := _runtime_stats.sprinting_speed

	if _weapon == null:
		transitioned.emit(self, StateMachine.STATES.SPRINTING)
		return

	if _input_component.get_active_input().attack or _weapon.is_attacking:
		speed = _runtime_stats.walking_speed

	var velocity := Vector2.ZERO

	if _input_component.get_active_input().direction != Vector2.ZERO:
		velocity = _input_component.get_active_input().direction * speed * delta
	else:
		velocity = _state_machine_owner.velocity.move_toward(
			Vector2.ZERO,
			speed * delta
		)

	_state_machine_owner.velocity = velocity
	_state_machine_owner.move_and_slide()

	_rotate_arm(delta)

	if _input_component.get_active_input().attack and _weapon.is_attacking:
		return

	if _input_component.get_active_input().attack:
		_weapon.attack()
		_timer_remain_in_combat.start(_time_remaining_in_combat_sec)
		return

	if _timer_remain_in_combat.is_stopped():
		_timer_remain_in_combat.start(_time_remaining_in_combat_sec)


func _rotate_arm(delta: float) -> void:
	var angle := _arm.get_angle_to(
		_state_machine_owner.get_global_mouse_position()
	)

	var ninety_deg_in_rad := 1.5708
	var revolutions_per_min := 2.0

	_arm.rotation = rotate_toward(
		_arm.rotation,
		_arm.rotation + angle + ninety_deg_in_rad,
		TAU * revolutions_per_min * delta
	)
