class_name Gathering extends PlayerState

@export var _hurtbox_component: ComponentWeaponHurtbox
@export var _interactor_component: ComponentInteractor
@export_range(0, 5, 1, "or_greater","suffix:sec") var _time_remaining_in_gathering_sec: int

var _tool: Weapon

#TODO might not be necessary
@onready var _timer_remain_in_gathering: Timer = $TimerRemainInGathering

func check_requirements() -> bool:
	var has_required_tool = _arm.get_required_tool(
		_get_closest_resource_node_required_tool_type()
	) != null

	return has_required_tool


func enter() -> void:
	print("player gathering state entered")

	_tool = _arm.get_required_tool(
		_get_closest_resource_node_required_tool_type()
	)

	_arm.show_tool(_tool)

	_hurtbox_component.hurt.connect(_on_hurtbox_hurt)


func exit() -> void:
	_hurtbox_component.hurt.disconnect(_on_hurtbox_hurt)

	_arm.show_weapon()


func physics_process(delta: float) -> void:
	super.physics_process(delta)

	var speed := _runtime_stats.sprinting_speed

	if _input_component.get_active_input().attack or _tool.is_attacking:
		speed = _runtime_stats.walking_speed

	var velocity := Vector2.ZERO

	if _input_component.get_active_input().direction != Vector2.ZERO:
		velocity = _input_component.get_active_input().direction * speed * delta
	else:
		velocity = _state_machine_owner.velocity.move_toward(
			Vector2.ZERO,
			speed * delta
		)

	_state_machine_owner.velocity = velocity
	_state_machine_owner.move_and_slide()

	_rotate_arm(delta)

	if _input_component.get_active_input().attack and _tool.is_attacking:
		return

	if _input_component.get_active_input().attack:
		#_arm.show_tool(_tool)
		_tool.attack()
		_timer_remain_in_gathering.start(_time_remaining_in_gathering_sec)

	if !_tool.is_attacking and _timer_remain_in_gathering.is_stopped():
		_timer_remain_in_gathering.start(_time_remaining_in_gathering_sec)


func _on_hurtbox_hurt() -> void:
	transitioned.emit(self, StateMachine.STATES.COMBAT)


func _get_closest_resource_node_required_tool_type() -> RItem.TOOL_TYPES:
	var resource_nodes: Array[ResourceNode]

	for interactable in _interactor_component.get_interactables():
		if interactable.interactable is not ResourceNode:
			continue

		resource_nodes.append(interactable.interactable)

	resource_nodes.sort_custom(func(rn_a: ResourceNode, rn_b: ResourceNode):
		var distance_to_a := rn_a.global_position.distance_to(
			_state_machine_owner.global_position
		)

		var distance_to_b := rn_b.global_position.distance_to(
			_state_machine_owner.global_position
		)

		return distance_to_a < distance_to_b
	)

	return resource_nodes[0].required_tool


func _on_timer_remain_in_gathering_timeout() -> void:
	transitioned.emit(self, StateMachine.STATES.SPRINTING)
