class_name PlayerState extends State

@export var _arm: PlayerArm

func physics_process(_delta: float) -> void:
	var next_state: StateMachine.STATES

	if _input_component.get_active_input().suicide:
		next_state = StateMachine.STATES.DEATH

	if next_state:
		transitioned.emit(self, next_state)


func _rotate_arm(_delta: float) -> void:
	var angle := _arm.get_angle_to(
		_state_machine_owner.get_global_mouse_position()
	)

	_arm.rotate(angle + deg_to_rad(90))
