class_name StateSprinting extends PlayerState

@export var _hurtbox_component: ComponentWeaponHurtbox
#@export var _interactor_component: ComponentInteractor

func _on_hurtbox_hurt() -> void:
	transitioned.emit(self, StateMachine.STATES.COMBAT)


func _on_resource_node_clicked() -> void:
	transitioned.emit(self, StateMachine.STATES.GATHERING)


func enter() -> void:
	print("player sprinting state entered")

	_hurtbox_component.hurt.connect(_on_hurtbox_hurt)
	#_interactor_component.resource_node_clicked.connect(_on_resource_node_clicked)


func exit() -> void:
	_hurtbox_component.hurt.disconnect(_on_hurtbox_hurt)
	#_interactor_component.resource_node_clicked.disconnect(_on_resource_node_clicked)


func physics_process(delta: float) -> void:
	super.physics_process(delta)

	var velocity := Vector2.ZERO
	var speed := _runtime_stats.sprinting_speed

	if _input_component.get_active_input().direction != Vector2.ZERO:
		velocity = _input_component.get_active_input().direction * speed * delta
	else:
		velocity = _state_machine_owner.velocity.move_toward(
			Vector2.ZERO,
			speed * delta
		)

	_state_machine_owner.velocity = velocity
	_state_machine_owner.move_and_slide()

	_rotate_arm(delta)

	if _input_component.get_active_input().attack:
		transitioned.emit(self, StateMachine.STATES.COMBAT)
	elif _is_idle():
		transitioned.emit(self, StateMachine.STATES.IDLE)


func _is_idle() -> bool:
	return _state_machine_owner.velocity == Vector2.ZERO and\
		_input_component.get_active_input().direction == Vector2.ZERO
