@tool
class_name ComponentAttractable extends Node

##The node that is attracted towards an attraction source
@export var _component_owner: Node2D
##Enables or disables the attraction for the attraciton target
@export var is_enabled: bool

@export var _communication_area_component: ComponentCommunicationArea
##The speed with witch the attraction target moves towards the attraction source
@export var _attraction_speed: float = 200


##A delay after spawn befor the attraction starts
var attraction_delay: float
##Set by interaction with a attractor component
var attraction_source: Node2D

var _attraction_start_time: float

func _ready() -> void:
	if Engine.is_editor_hint() and _component_owner == null:
		_component_owner = get_parent()

	_communication_area_component.attractor_area_entered.connect(func(actor):
		attraction_source = actor
	)
	_communication_area_component.attractor_area_exited.connect(func(_actor):
		attraction_source = null
	)

	_attraction_start_time = Time.get_unix_time_from_system() + attraction_delay


func _physics_process(delta: float) -> void:
	if !is_enabled or attraction_source == null:
		return

	if Time.get_unix_time_from_system() < _attraction_start_time:
		return

	_move_towards_attraction_source(delta)


func _move_towards_attraction_source(delta: float) -> void:
	var new_pos := _component_owner.global_position.move_toward(
		attraction_source.global_position,
		_attraction_speed * delta
	)

	_component_owner.global_position = new_pos
