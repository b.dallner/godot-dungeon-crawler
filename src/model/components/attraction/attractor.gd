@tool
class_name ComponentAttractor extends Area2D

@export var actor: Node2D

func _ready() -> void:
	if Engine.is_editor_hint() and actor == null:
		actor = get_parent()
