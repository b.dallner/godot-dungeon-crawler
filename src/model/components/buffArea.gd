class_name ComponentBuffArea extends Area2D

@export var _buffs_in_range: Array[RStatsBuff]
@export var _buffs_on_exited: Array[RStatsBuff]
@export_range(1, 5, 1, "or_greater", "suffix:sec") var _time_till_buff_sec: float
@export var _remove_buffs_on_exit_on_area_entered: bool

var _players_in_range_timing: Dictionary = {}

@onready var _timer_buff_check: Timer = $TimerBuffCheck

func _on_body_entered(body: CharacterBody2D) -> void:
	if body is Player:
		_players_in_range_timing[body.name] = PlayerTimeTracker.new(body)


func _on_body_exited(body: CharacterBody2D) -> void:
	if body is Player:
		var tracker: PlayerTimeTracker = _players_in_range_timing[body.name]

		_players_in_range_timing.erase(body.name)

		for buff in _buffs_in_range:
			tracker.player.remove_buff(buff)

		if tracker.time >= _time_till_buff_sec:
			for buff in _buffs_on_exited:
				tracker.player.add_buff(buff)


func _on_timer_buff_check_timeout() -> void:
	if _players_in_range_timing.size() > 0:
		for tracker: PlayerTimeTracker in _players_in_range_timing.values():
			tracker.time += _timer_buff_check.wait_time

			if tracker.time < _time_till_buff_sec:
				continue

			for buff in _buffs_in_range:
				if _remove_buffs_on_exit_on_area_entered:
					for buff2 in _buffs_on_exited:
						tracker.player.remove_buff(buff2)

				tracker.player.add_buff(buff)


class PlayerTimeTracker:
	var player: Player
	var time: float

	@warning_ignore("shadowed_variable")
	func _init(player: Player) -> void:
		self.player = player
