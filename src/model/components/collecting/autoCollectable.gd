@tool
class_name ComponentAutoCollectable extends Node

signal state_updated()

##The collectable to be collected when in contact with a collector
@export var collectable: Node2D

##Enables or disables the ability to collect the collectable
@export var is_enabled: bool:
	set(value):
		is_enabled = value
		state_updated.emit()

func _ready() -> void:
	if Engine.is_editor_hint() and collectable == null:
		collectable = get_parent()
