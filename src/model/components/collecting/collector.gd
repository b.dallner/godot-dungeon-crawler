@tool
class_name ComponentCollector extends Area2D

@export var _actor: Node2D
@export var _inventory_component: ComponentInventory

func _ready() -> void:
	if Engine.is_editor_hint() and _actor == null:
		_actor = get_parent()


func _on_area_entered(area: Area2D) -> void:
	if area is ComponentCommunicationArea:
		var cac := area as ComponentCommunicationArea

		if cac.auto_collectable_component != null:
			cac.auto_collectable_component.state_updated.connect(
				_collect_item.bind(cac.auto_collectable_component)
			)

		_collect_item(cac.auto_collectable_component)


func _collect_item(auto_collectable_component: ComponentAutoCollectable) -> void:
	var is_auto_collectable = auto_collectable_component != null \
		and auto_collectable_component.is_enabled

	if !is_auto_collectable:
		return

#TODO BUG if player inventory is full drop item again
#func _try_interaction(interactable: ComponentInteractable) -> void:
	#if interactable.interactable is Item:
		#if inventory_component.add_item(interactable.interactable):
			#_remove_interactable(interactable)

	_inventory_component.add_item_node(auto_collectable_component.collectable)


func _on_area_exited(area: Area2D) -> void:
	if area is ComponentCommunicationArea:
		var cac := area as ComponentCommunicationArea

		if cac.auto_collectable_component != null:
			cac.auto_collectable_component.state_updated.disconnect(_collect_item)
