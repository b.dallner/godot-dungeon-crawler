class_name ComponentAggro extends Area2D

signal target_changed(target: Node2D)

#@export var _is_aggresiv: bool
#@export var _aggro_duration: float = 5.0

var _aggressors: Array[AggroTracker]
var _current_target: Node2D

func _physics_process(_delta: float) -> void:
	for tracker: AggroTracker in _aggressors:
		tracker.proximity_aggro = global_position.distance_to(
			tracker.aggressor.global_position
		)


func _on_body_entered(_body: Node2D) -> void:
	pass
	#if body is Player and _is_aggresiv:
		#calc_aggro(body, 0.0)


func _on_body_exited(_body: Node2D) -> void:
	pass
	#if body is Player:
		#await get_tree().create_timer(_aggro_duration).timeout
#
		#_reset_aggro_for(body)


func add_targets(targets: Array[Node2D]) -> void:
	for target in targets:
		var tracker = AggroTracker.new()
		tracker.aggressor = target
		tracker.aggro = 0.0

		_aggressors.append(tracker)


func calc_aggro(aggressor: Node2D, interaction_aggro_value: float) -> void:
	var array := _aggressors.filter(func(tracker: AggroTracker):
		return tracker.aggressor == aggressor
	) as Array[AggroTracker]

	if array.size() == 0:
		var tracker = AggroTracker.new()
		tracker.aggressor = aggressor
		tracker.aggro = interaction_aggro_value

		_aggressors.append(tracker)
	else:
		array[0].aggro += interaction_aggro_value

	_decide_target()


func get_current_target() -> Node2D:
	if _current_target == null:
		_decide_target()

	return _current_target


func _decide_target() -> void:
	if _aggressors.size() == 0:
		_current_target = null
		return

	_aggressors.sort_custom(func(a: AggroTracker, b: AggroTracker):
		return a.get_aggro() > b.get_aggro()
	)

	var target := _aggressors[0].aggressor

	if _current_target != target:
		_current_target = target
		target_changed.emit(target)


#func _reset_aggro_for(player: Player) -> void:
	#for i in _aggressors.size():
		#if _aggressors[i].aggressor == player:
			#_aggressors.remove_at(i)
			#break
#
	#_decide_target()


class AggroTracker:
	var aggressor: Node2D
	var proximity_aggro: float
	var aggro: float

	func get_aggro() -> float:
		return proximity_aggro + aggro


	func _to_string() -> String:
		return aggressor.name + ": " + str(aggro)
