class_name ComponentHitbox extends Area2D

@export var _weapon: Weapon
@export var _tool_component: ComponentTool

func _on_area_entered(area: Area2D) -> void:
	if area is ComponentWeaponHurtbox:
		##Round to one decimal
		var damage := Math.round_decimals(_weapon.get_damage(), 1)

#TODO add or subtract damage buffs or debuffs

		area.hit(damage, _weapon.possessor)

	elif area is ComponentToolHurtbox and _tool_component != null:
		area.harvest(_weapon)
