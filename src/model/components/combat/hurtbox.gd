class_name ComponentHurtbox extends Area2D

@warning_ignore("unused_signal")
signal hurt(damage: float)
