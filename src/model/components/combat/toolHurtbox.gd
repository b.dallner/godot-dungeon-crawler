class_name ComponentToolHurtbox extends ComponentHurtbox

@export var _resource_node: ResourceNode

func harvest(weapon: Weapon) -> void:
	var tool_damage := weapon.get_tool_tier() - _resource_node.required_tier
	tool_damage = maxi(tool_damage, -1)

	hurt.emit(tool_damage)
