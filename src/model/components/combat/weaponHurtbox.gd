class_name ComponentWeaponHurtbox extends ComponentHurtbox

@export var _stats_component: ComponentStats
@export var _aggro_component: ComponentAggro
@export var _particle_spawner: GPUParticles2D

func hit(damage: float, agressor: Node2D) -> void:
	if _aggro_component != null:
		_aggro_component.calc_aggro(agressor, damage)

	if _stats_component.get_stats().is_invincible:
#TODO comunicate to the player that the traget is invincible
		return

	if _particle_spawner != null:
		_spawn_hurt_particals(agressor.global_position)

	_stats_component.take_damage(damage)

	hurt.emit(damage)


func _spawn_hurt_particals(agressor_pos: Vector2) -> void:
	_particle_spawner.restart()
	##Add PI to rotate the angle of the source by 180 degrees
	_particle_spawner.rotation = get_angle_to(agressor_pos) + PI
	_particle_spawner.emitting = true
