class_name ComponentInvisibility extends Node

@export var _texture: Sprite2D
@export var _stats: ComponentStats
@export var _material: ShaderMaterial

func _ready() -> void:
	_stats.get_stats().invisiblility_changed.connect(_invisible)


func _invisible() -> void:
	_texture.material = _material
	_material.set_shader_parameter(
		"Enabled",
		_stats.get_stats().is_invisible
	)
