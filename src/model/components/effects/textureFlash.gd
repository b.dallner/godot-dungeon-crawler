class_name ComponentTextureFlash extends Node

@export var _texture: Sprite2D
@export_range(0.0, 1.0, 0.05) var _flash_duration: float = 0.10
@export var _material: ShaderMaterial

@export var _hurtbox_component: ComponentHurtbox

func _ready() -> void:
	if _hurtbox_component != null:
		_hurtbox_component.hurt.connect(_flash)


func _flash(_damage: float) -> void:
	_texture.material = _material
	_material.set_shader_parameter("Enabled", true)

	await get_tree().create_timer(_flash_duration).timeout

	_material.set_shader_parameter("Enabled", false)
