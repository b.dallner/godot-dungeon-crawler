class_name ComponentTextureShake extends Node

@export var _texture: Sprite2D

@export var _hurtbox_component: ComponentHurtbox

func _ready() -> void:
	if _hurtbox_component != null:
		_hurtbox_component.hurt.connect(shake)


func shake(_damage: float) -> void:
	var tween := get_tree().create_tween()
	tween.tween_property(_texture, "rotation", -0.12, 0.05)
	tween.tween_property(_texture, "rotation", 0.12, 0.1)
	tween.tween_property(_texture, "rotation", 0.0, 0.05)
