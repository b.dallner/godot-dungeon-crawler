class_name ComponentClickabelSprite2D extends Sprite2D

signal clicked()

func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.is_action_pressed("left_click"):
			if is_pixel_opaque(get_local_mouse_position()):
				clicked.emit()
