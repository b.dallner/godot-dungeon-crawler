@tool #only a tool becaus of in editor tooling dependencies
class_name ComponentCommunicationArea extends Area2D

signal attractor_area_entered(attraction_actor: Node2D)
signal attractor_area_exited(attraction_actor: Node2D)

#signal interactor_area_entered(interaction_actor: ComponentInteractor)
#signal interactor_area_exited(interaction_actor: ComponentInteractor)

@export var attractable_component: ComponentAttractable
@export var auto_collectable_component: ComponentAutoCollectable
#@export var interactable_component: ComponentInteractable

func _on_area_entered(area: Area2D) -> void:
	if area is ComponentAttractor:
		attractor_area_entered.emit(area.actor)
	#elif area is ComponentInteractor:
		#interactor_area_entered.emit(area)


func _on_area_exited(area: Area2D) -> void:
	if area is ComponentAttractor:
		attractor_area_exited.emit(area.actor)
	#elif area is ComponentInteractor:
		#interactor_area_exited.emit(area)
