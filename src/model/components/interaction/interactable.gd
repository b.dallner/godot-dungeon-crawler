#TODO key interactions with resource nodes put the player immediately into the
#gathering state
@tool
class_name ComponentInteractable extends Area2D

signal focus_changed(has_focus: bool)
signal interaction_conditions_triggered()
signal state_changed(state: bool)

var has_focus: bool = false:
	set(value):
		has_focus = value
		focus_changed.emit(value)

@export var interactable_owner: Node2D
@export var is_enabled: bool:
	set(value):
		is_enabled = value
		state_changed.emit(value)

#TBD check if this is a good solution once we now how to spawn e.g. crates and
#how to set interaction restriction via code
@export var interaction_required_items: Array[RItemRequirement] = []
@export var interaction_required_nodes: Array[Node2D] = []

var _interaction_locks: Array[ComponentInteractionLock] = []
var _interactions: Array[RInteractionInput] = []

func _ready() -> void:
	for required_node in interaction_required_nodes:
		var interaction_locks = required_node.get_children().filter(func(child):
			return child is ComponentInteractionLock
		)

		_interaction_locks.append_array(interaction_locks)

	if Engine.is_editor_hint():
		return

	for lock in _interaction_locks:
		lock.condition_changed.connect(_on_interaction_lock_condition_changed)

	if interaction_required_items.size() > 0:
		SignalBus.ui_item_requirements_requested.emit(
			interaction_required_items, self
		)


func _on_interaction_lock_condition_changed(is_active_trigger: bool) -> void:
	if !is_active_trigger or !_check_required_nodes():
		return

	interaction_conditions_triggered.emit()


func register_interaction(input: RInteractionInput, callback: Callable) -> void:
	##This is required for the triggered signal to work. If we don't duplicate
	##the resource, its shared via referenc, it will trigger all objects of the
	##same typ at once
	input = input.duplicate()
	input.triggered.connect(callback)

	_interactions.append(input)


func get_interaction_inputs() -> Array[RInteractionInput]:
	return _interactions


func _check_required_nodes() -> bool:
	return _interaction_locks.all(func(lock: ComponentInteractionLock):
		return lock.is_condition_meet
	)
