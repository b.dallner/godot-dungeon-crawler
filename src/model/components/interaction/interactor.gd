@tool
class_name ComponentInteractor extends Area2D

signal interactable_in_range(interactable: ComponentInteractable)
signal interactable_out_of_range(interactable: ComponentInteractable)

@export var _actor: Actor
@export var _input_component: ComponentInput

var _interactables: Array[ComponentInteractable]

func _physics_process(_delta: float) -> void:
	if _interactables.size() == 0:
		return

#TODO we could ask the focus manager instead
	var index := _interactables.find_custom(func(i: ComponentInteractable):
		return i.has_focus
	)

	if index < 0:
		return

	var interactions = _interactables[index].get_interaction_inputs()
	var input := _input_component.get_active_input()

	for interaction: RInteractionInput in interactions:
		if input.interact:
			var action := InputMapping.get_action_name(interaction.input)

			if input[action]:
				interaction.triggered.emit(_actor)


func _on_area_entered(area: Area2D) -> void:
	if area is not ComponentInteractable:
		return

	if !area.is_enabled:
		return

	_interactables.append(area)
	interactable_in_range.emit(area)


func _on_area_exited(area: Area2D) -> void:
	if area is not ComponentInteractable:
		return

	if !area.is_enabled:
		return

	_interactables.remove_at(_interactables.find(area))
	interactable_out_of_range.emit(area)
