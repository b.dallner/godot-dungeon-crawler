class_name NodeCondition extends Node

signal status_changed()

@export var is_condition_meet: bool:
	set(value):
		is_condition_meet = value
		status_changed.emit()
