class_name NodeRequirement extends Node

signal requirement_condition_changed(condition_status: bool)

@export var _condition: NodeCondition
@export var _is_active_trigger: bool

var is_condition_meet: bool

func _ready() -> void:
	_condition.status_changed.connect(_on_requirement_status_changed)


func _on_requirement_status_changed() -> void:
	is_condition_meet = _condition.is_condition_meet

	if !_is_active_trigger:
		return

	requirement_condition_changed.emit(is_condition_meet)
