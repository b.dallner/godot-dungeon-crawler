@tool
class_name ComponentLoot extends Node2D

@export var _node_owner: Node2D

@export_category("Loot")
@export var _loot: Array[RLoot]
#TODO implement rng loot functionality
@export var _rng_loot: Array[RRngLoot] = []


func create_loot_items() -> void:
	var loot := _loot

	SignalBus.create_loot_requested.emit(loot, _node_owner.global_position)
