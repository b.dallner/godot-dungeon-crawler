class_name ComponentLootProgress extends Node

@export var _node_owner: Node2D
@export var _loot_drops_by_hp: Array[RLootProgress]

func create_loot_items_by_step_value(value: int) -> void:
	var loot: Array[RLoot]

	for step: RLootProgress in _loot_drops_by_hp.duplicate():
		if step.value < value:
			break

		loot.append(step.loot)

		_loot_drops_by_hp.pop_front()

	if loot.size() == 0:
		return

	SignalBus.create_loot_requested.emit(loot, _node_owner.global_position)
