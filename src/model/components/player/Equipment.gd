class_name ComponentEquipment extends Node

var equipment: REquipment

@export var _inventory_component: ComponentInventory
@export var _arm: PlayerArm

func _ready() -> void:
	SignalBus.ui_inventory_item_equipped.connect(_on_item_equipped)
	SignalBus.ui_inventory_item_unequipped.connect(_on_item_unequipped)


func _on_item_equipped(item: RItemEquipment) -> void:
	var prev_item := equipment.equip_or_swap_item(item)

	if item is RItemWeapon:
		if item.slot_type == RItem.SLOT_TYPES.WEAPON:
			_arm.equip_weapon(item)
		else:
			_arm.equip_tool(item)

	_inventory_component.remove_item_quan(RItemQuan.create_instance(item))

	if prev_item != null:
		_inventory_component.add_item_resource(prev_item)


func _on_item_unequipped(item: RItemEquipment) -> void:
	if item is RItemWeapon:
		if item.slot_type == RItem.SLOT_TYPES.WEAPON:
			_arm.unequip_weapon()
		else:
			_arm.unequip_tool(item)

	equipment.unequip_item(item)

	_inventory_component.add_item_resource(item)
