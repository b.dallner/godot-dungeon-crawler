class_name ComponentInput extends Node

var _active_input: MyInput

func _ready() -> void:
	for c:MyInput in get_children():
		if !c.is_default:
			continue

		_active_input = c
		break


func _physics_process(_delta: float) -> void:
	_active_input.process()


func get_active_input() -> MyInput:
	return _active_input
