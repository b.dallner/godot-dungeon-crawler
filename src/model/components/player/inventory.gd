class_name ComponentInventory extends Node

var inventory: RInventory

func get_keys() -> Array[RItemQuan]:
	return inventory.keys


func get_currency() -> float:
	return inventory.currency


func get_items() -> Array[RItemQuan]:
	return inventory.items#


func add_item_node(item: Item) -> bool:
	var item_resource := item.data

	if item_resource is RItemCurrency:
		change_currency(item.get_currency_value())
	else:
		if !add_item_resource(item_resource):
			return false

	SoundBus.play_pickup_sound(item_resource.pickup_sound)
	item.queue_free()

	return true


func add_item_resource(item: RItem) -> bool:
	if item is RItemKey:
		_add_key(item)
		return true

	return _add_item(item)


func change_currency(value: float) -> void:
	inventory.currency += value
	inventory.currency_changed.emit()


func remove_item_quan(item_quan: RItemQuan) -> bool:
	#var backup: Array[RItemQuan] = []

	var matching_items := inventory.items.filter(func(inv_item_quan: RItemQuan):
		var is_matching := inv_item_quan.item == item_quan.item

		#if is_matching:
			#backup.append(inv_item.duplicate())

		return is_matching
	) as Array[RItemQuan]

	if matching_items.size() == 0:
		return false

	matching_items[0].quantity -= 1

	if matching_items[0].quantity <= 0:
		inventory.items.remove_at(inventory.items.find(matching_items[0]))

	inventory.items_changed.emit()

	return true


func remove_key(item_qaun: RItemQuan) -> void:
	var key = inventory.keys.filter(func(k): return k.item == item_qaun.item)[0]
	key.quantity -= item_qaun.quantity

	inventory.keys_changed.emit()


#func remove_keys(keys: Array[RItemQuan]) -> void:
	#for item_qaun in keys:
		#var key = _inventory.keys.filter(
			#func(k): return k.item == item_qaun.item
		#)[0]
#
		#key.quantity -= item_qaun.quantity
#
	#_inventory.keys_changed.emit()


func has_item_quan(item_quan: RItemQuan) -> bool:
	if item_quan.item is RItemCurrency:
		return inventory.currency >= item_quan.quantity

	elif item_quan.item is RItemKey:
		var has_keys := inventory.keys.any(func(inv_item: RItemQuan):
			return (
				inv_item.item == item_quan.item and
				inv_item.quantity >= item_quan.quantity
			)
		)

		return has_keys

	else:
		var has_items := inventory.items.any(func(inv_item: RItemQuan):
			return (
				inv_item.item == item_quan.item and
				inv_item.quantity >= item_quan.quantity
			)
		)

		return has_items


func _add_item(item: RItem) -> bool:
	var matching_items: Array[RItemQuan]
	var valid_item_quans: Array[RItemQuan]

	if item.is_stackable:
		matching_items = inventory.items.filter(func(inv_item: RItemQuan):
			return inv_item.item == item and inv_item.quantity < item.max_stack_size
		)

		for matching_item in matching_items:
			if matching_item.quantity >= item.max_stack_size:
				continue

			valid_item_quans.append(matching_item)

	if valid_item_quans.size() == 0:
#TODO check max inventory slots and if full return false
		var new_item_qaun := RItemQuan.create_instance(item)

		inventory.items.append(new_item_qaun)
	else:
#TBD split picked up item over stacks if item.quantity is to large
		var i := valid_item_quans[0]
		i.quantity += 1

	inventory.items_changed.emit()

	return true


func _add_key(key: RItemKey) -> void:
	var matching_keys: Array[RItemQuan] = inventory.keys.filter(
		func(k: RItemQuan): return k.item == key
	)

	if matching_keys.size() == 1:
		matching_keys[0].quantity += 1
	else:
		var loot = RItemQuan.create_instance(key)

		inventory.keys.append(loot)

	inventory.keys_changed.emit()
