class_name State extends Node

@warning_ignore("unused_signal")
signal transitioned(state: State, next_state: State)

var _input_component: ComponentInput
var _state_machine_owner: CharacterBody2D

@export var state_name: StateMachine.STATES

var _runtime_stats: RRuntimeStats

func init(stats: RRuntimeStats, input_component: ComponentInput, state_machine_owner: CharacterBody2D) -> void:
	_runtime_stats = stats
	_input_component = input_component
	_state_machine_owner = state_machine_owner


func check_requirements() -> bool:
	return true


func enter() -> void:
	pass


func exit() -> void:
	pass


func physics_process(_delta: float) -> void:
	pass
