class_name StateMachine extends Node

enum STATES{
	SPAWNING,
	CHASING,
	ATTACKING,
	IDLE,
	#WALKING,
	SPRINTING,
	COMBAT,
	GATHERING,
	DEATH,
}

@export var _actor: Actor
@export var _stats_component: ComponentStats
@export var _input_component: ComponentInput
@export var _inital_state: State

var _states: Dictionary = {}
var _current_state: State

func _physics_process(delta: float) -> void:
	if _current_state == null:
		return

	_current_state.physics_process(delta)


func _change_state(old_state: State, next_state_name: STATES) -> void:
	##State changed got called from a diffrent state than the current one
	if old_state != _current_state:
		print_stack()
		print("Invalid state change from " + old_state.name)
		return

	var new_state := _states[next_state_name] as State

	if !new_state.check_requirements():
		return

	if _current_state != null:
		_current_state.exit()

	new_state.enter()

	_current_state = new_state


func init() -> void:
	_stats_component.zero_hp.connect(func():
		_change_state(_current_state, StateMachine.STATES.DEATH)
	)

	for state in get_children():
		if state is State:
			state.init(_stats_component.get_stats(), _input_component, _actor)

			_states[state.state_name] = state
			state.transitioned.connect(_change_state)

	if _inital_state != null:
		_inital_state.enter()
		_current_state = _inital_state
