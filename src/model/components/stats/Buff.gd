class_name Buff extends Node

signal buff_timeout(buff: Buff)
signal buff_ticked(stats_effect: RStatsEffect)

const BUFF = preload("res://src/model/components/stats/Buff.tscn")

var stats_buff: RStatsBuff

var _count_ticks: int
var _max_ticks: int
var _cached_stat_effects: Array[RStatsEffect]

@onready var _lifetime: Timer = $Lifetime
@onready var _tick: Timer = $Tick

static func create(stats_buff_data: RStatsBuff) -> Buff:
	var buff = BUFF.instantiate()
	buff.stats_buff = stats_buff_data

	return buff


func _ready() -> void:
	if stats_buff.duration_sec == -1:
		_max_ticks = 1
	else:
		##-1, because we start counting ticks with 0 e.g. 5 tickets = 0,1,2,3,4
		_max_ticks = ceili(stats_buff.duration_sec / stats_buff.tick_speed_sec) #- 1

		_lifetime.start(stats_buff.duration_sec)
		_lifetime.timeout.connect(_on_lifetime_timeout)

	stats_buff.buff_reapplied.connect(func():
		_lifetime.start(stats_buff.duration_sec)
		_count_ticks = 0
	)

	if stats_buff.is_spread_over_time:
		_tick.start(stats_buff.tick_speed_sec)
		_tick.timeout.connect(_on_tick_timeout)

	_cache_stat_effects()


#func _physics_process(_delta: float) -> void:
	#stats_buff.remaining_duration = _lifetime.time_left


func _on_lifetime_timeout() -> void:
	buff_timeout.emit(self)

	var is_last_tick = _count_ticks + 1 == _max_ticks

	##If we prematurely kill the buff, we don't wan't it to continue ticking
	##in the background
	if is_last_tick:
		_tick.one_shot = true
		_tick.timeout.connect(queue_free)
	else:
		_tick.stop()
		queue_free()


func _on_tick_timeout() -> void:
	if stats_buff.duration_sec == -1:
		_count_ticks = 0

	buff_ticked.emit(_cached_stat_effects[_count_ticks])
	_count_ticks += 1


func _cache_stat_effects() -> void:
	var sum_values := {}

	for i in _max_ticks:
		var stats_effect = RStatsEffect.new()

		for p in ResourceUtil.get_script_property_list(stats_effect.effected_stats):
			if p.type == TYPE_FLOAT or p.type == TYPE_INT:
				var value_percantage := stats_buff.spread_curve.sample_baked(
					1.0 / _max_ticks * i
				)

				stats_effect.effected_stats[p.name] = roundf(
					stats_buff.effected_stats[p.name] * value_percantage / 100
				)
				stats_effect.is_percentage = stats_buff.is_percantage

				if !sum_values.has(p.name):
					sum_values[p.name] = 0.0

				sum_values[p.name] += stats_effect.effected_stats[p.name]

		_cached_stat_effects.append(stats_effect)

	for p in sum_values.keys():
		var diff: float = stats_buff.effected_stats[p] - sum_values[p]
		_cached_stat_effects[0].effected_stats[p] += diff


func get_remaining_lifetime() -> float:
	return _lifetime.time_left
