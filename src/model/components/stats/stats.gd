class_name ComponentStats extends Node

signal zero_hp()

var _base_stats: RStats
var _cached_stats: RRuntimeStats
var _stats_buffs: Array[RStatsBuff]

@export var _actor: Actor

func _ready() -> void:
	var data := _actor.data as RActor

	_base_stats = data.base_stats
	_cached_stats = _actor.runtime_stats

	_calculate_stats_cache()
	_base_stats.changed.connect(_calculate_stats_cache)

#TODO check if needed anymore
	_cached_stats.max_hp_changed.connect(func():
		##Trigger for current hp validation
		_cached_stats.current_hp = _cached_stats.current_hp
	)

	assert(_base_stats != null, "No entity data on parent found!")


func take_damage(damage: float) -> void:
	_cached_stats.current_hp -= damage

	if is_zero_approx(_cached_stats.current_hp) or _cached_stats.current_hp < 0.0:
		zero_hp.emit()


func apply_stats_effect(stats_changes: RStatsEffect) -> bool:
	var are_stats_changed := false

	if stats_changes.is_percentage:
		are_stats_changed = _apply_specific_stat_changes(stats_changes)
	else:
		are_stats_changed = _apply_generic_stat_changes(stats_changes)

	_cached_stats.validate_current_hp()

	return are_stats_changed


func apply_stats_buff(stats_buff: RStatsBuff) -> bool:
	#var are_stats_changed := false

	#if stats_changes is RStatsEffect:
		#if stats_changes.is_percentage:
			#are_stats_changed = _apply_specific_stat_changes(stats_changes)
		#else:
			#are_stats_changed = _apply_generic_stat_changes(stats_changes)
#
		#_cached_stats.validate_current_hp()
#
	#elif stats_changes is RStatsBuff:
		#are_stats_changed = true
#
	#return are_stats_changed

	var is_new_buff := false
	var index := _stats_buffs.find(stats_buff)

	if index >= 0:
		_stats_buffs[index].buff_reapplied.emit()
	else:
		_stats_buffs.append(stats_buff)
		is_new_buff = true

	_calculate_stats_cache()

	return is_new_buff


func remove_stats_buff(stats_buff: RStatsBuff) -> void:
	var index := _stats_buffs.find(stats_buff)

	if index < 0:
		return

	_stats_buffs.erase(stats_buff)

	_calculate_stats_cache()


func get_stats() -> RRuntimeStats:
	return _cached_stats


#func _apply_buff(stats_buff: RStatsBuff) -> void:
	#var index := _stats_buffs.find(stats_buff)
#
	#if index >= 0:
		#_stats_buffs[index].buff_reapplied.emit()
	#else:
		#_stats_buffs.append(stats_buff)
#
	#_calculate_stats_cache()


func _apply_generic_stat_changes(stats_effect: RStatsEffect) -> bool:
	return _apply_stats_changes(_cached_stats, stats_effect.effected_stats)


func _apply_specific_stat_changes(stats_effect: RStatsEffect) -> bool:
	var effected_stats := stats_effect.effected_stats
	var num_changed_stats := 0

	if effected_stats.current_hp != 0 and \
		_cached_stats.current_hp < _cached_stats.max_hp:

		_cached_stats.current_hp += _cached_stats.max_hp * effected_stats.current_hp / 100

		num_changed_stats += 1

	return num_changed_stats != 0


func _calculate_stats_cache() -> void:
	_reset_stat_change(_cached_stats, _base_stats)

	for buff in _stats_buffs:
		if buff.is_spread_over_time:
			continue

		if buff.is_percantage:
			_apply_stats_changes_percantage(buff.effected_stats)
		else:
			#_apply_stats_changes(_cached_stats.stats, buff.effected_stats.stats)
			_apply_stats_changes(_cached_stats, buff.effected_stats)

	_cached_stats.validate_current_hp()


func _apply_stats_changes(stats_a: RRuntimeStats, stats_b: RStats) -> bool:
	var num_changed_stats := 0

	for p in ResourceUtil.get_script_property_list(stats_a):
		var prev_value = stats_a[p.name]

		if p.type == TYPE_FLOAT or p.type == TYPE_INT:
			stats_a[p.name] += stats_b[p.name]
		elif p.type == TYPE_BOOL:
			stats_a[p.name] = stats_a[p.name] or stats_b[p.name]
		else:
			stats_a[p.name] = stats_b[p.name]

		if prev_value != stats_a[p.name]:
			num_changed_stats += 1

	return num_changed_stats > 0


func _apply_stats_changes_percantage(buff_stats: RRuntimeStats) -> void:
	#for p in ResourceUtil.get_script_property_list(_cached_stats):
		#if p.type == TYPE_FLOAT or p.type == TYPE_INT:
			#_cached_stats.stats[p.name] += \
				#_base_stats[p.name] * buff_stats.stats[p.name] / 100

	for p in ResourceUtil.get_script_property_list(_cached_stats):
		if p.type == TYPE_FLOAT or p.type == TYPE_INT:
			_cached_stats[p.name] += \
				_cached_stats[p.name] * buff_stats[p.name] / 100


func _reset_stat_change(stats_a: RRuntimeStats, stats_b: RStats):
#TODO do i have to reset all the runtime stats or just the inherited once?
	#var empty_runtime_stats = RRuntimeStats.new()

	#for p in ResourceUtil.get_script_property_list(empty_runtime_stats):
		#stats_a[p.name] = empty_runtime_stats[p.name]

	for p in ResourceUtil.get_script_property_list(stats_b):
		stats_a[p.name] = stats_b[p.name]
