@tool #only a tool becaus of in editor tooling dependencies
class_name MyButton extends Node2D

@export var _interaction_sound_on: AudioStreamWAV
@export var _interaction_sound_off: AudioStreamWAV
@export var _interaction_input: RInteractionInput = RInteractionInput.new()
@export var _is_activated: bool

#TODO what about one time use and timer based reset?

@onready var _animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D
@onready var _interactable_component: ComponentInteractable = $InteractableComponent
@onready var _interaction_sound: AudioStreamPlayer2D = $InteractionSound
@onready var _interaction_lock_component: ComponentInteractionLock = $InteractionLockComponent


func _ready() -> void:
	if Engine.is_editor_hint():
		return

	_setup_button()


func _toggle_button(_actor: Actor) -> void:
	_is_activated = !_is_activated

	if _is_activated:
		_animated_sprite_2d.play()
		_interaction_sound.stream = _interaction_sound_on
	else:
		_animated_sprite_2d.frame = 0
		_interaction_sound.stream = _interaction_sound_off

	_interaction_lock_component.is_condition_meet = _is_activated
	_interaction_sound.play()


func _setup_button() -> void:
	_interactable_component.register_interaction(
		_interaction_input, _toggle_button
	)

	if _is_activated:
		_animated_sprite_2d.play()



#func _on_interactable_component_focus_changed(in_focus: bool) -> void:
	#if in_focus:
		#_interactable_component.hint.modulate = Color.WHITE
	#else:
		#_interactable_component.hint.modulate = Color.BLACK
