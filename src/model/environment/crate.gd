class_name Crate extends StaticBody2D

@export var _interaction_input: RInteractionInput

@onready var _sprite_2d: Sprite2D = $Sprite2D
@onready var _interactable_component: ComponentInteractable = $InteractableComponent
@onready var _interaction_sound: AudioStreamPlayer2D = $InteractionSound
@onready var _loot_component: ComponentLoot = $LootComponent
@onready var _texture_shake_component: ComponentTextureShake = $ComponentTextureShake

func _ready() -> void:
	#_interactable_component.interaction_denied.connect(func():
		#_texture_shake_component.shake(0.0)
	#)

	_interactable_component.register_interaction(
		_interaction_input, _open_crate_by_actor
	)

#TODO currently allows to open the chest as often as you want
	_interactable_component.interaction_conditions_triggered.connect(
		_open_crate
	)


#func _on_interaction_successful(interaction_input: RInteractionInput, _interactor: ComponentInteractor) -> void:
	#if interaction_input.input == InputMapping.INTERACTION_INPUTS.INTERACT:

		#if !_interactor.check_owner_has_required_items(_interactable_component._interaction_required_items):
			#_texture_shake_component.shake(0.0)
			#return
#
		#_interactable_component.consume_required_items()
		#_open_crate()

func _open_crate_by_actor(actor: Player) -> void:
	var required_items = _interactable_component.interaction_required_items

	if !actor.check_required_items(required_items):
		_texture_shake_component.shake(0.0)
		return

	var item_quans: Array[RItemQuan] = []

	for item in required_items:
		if !item.is_item_consume_on_interaction:
			continue

		item_quans.append(item.item_quan)

	actor.remove_items(item_quans)

	_interactable_component.interaction_required_items.clear()

	_open_crate()


func _open_crate() -> void:
	SignalBus.ui_item_requirements_removed.emit(_interactable_component)

	collision_layer = 0
	_sprite_2d.frame = 1

	_interaction_sound.play()
	_loot_component.create_loot_items()
	_interactable_component.is_enabled = false


#func _on_node_condition_meet(trigger_type: NodeRequirement.TriggerType) -> void:
	#if trigger_type == NodeRequirement.TriggerType.INTERACT:
		#_open_crate()
