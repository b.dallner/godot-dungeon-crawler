class_name ComponentInteractionLock extends Node

signal condition_changed(is_active_trigger)

@export var _is_active_trigger: bool

var is_condition_meet: bool:
	set(value):
		is_condition_meet = value
		condition_changed.emit(_is_active_trigger)
