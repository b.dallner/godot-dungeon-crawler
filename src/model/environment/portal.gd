class_name Portal extends StaticBody2D

enum PORTAL_STATE{
	idle,
	active,
}

var _state: PORTAL_STATE = PORTAL_STATE.idle

@export var _interaction_input: RInteractionInput

@onready var _animation_player: AnimationPlayer = $AnimationPlayer
@onready var _interactable_component: ComponentInteractable = $InteractableComponent

func _ready() -> void:
	_interactable_component.register_interaction(
		_interaction_input, _use_portal
	)

	#_interactable_component.interaction_conditions_triggered.connect(
		#_use_portal
	#)


#func _on_interactor_in_range(_interactor: ComponentInteractor) -> void:
	#change_state(PORTAL_STATE.active)


#func _on_interactor_out_of_range(_interactor: ComponentInteractor) -> void:
	#change_state(PORTAL_STATE.idle)


#func _on_interaction_successful(interaction_input: RInteractionInput, _interactor: ComponentInteractor) -> void:
	#if interaction_input.input == InputMapping.INTERACTION_INPUTS.INTERACT:
		#_use_portal()


func change_state(state: PORTAL_STATE) -> void:
	_state = state

	if state == PORTAL_STATE.active:
		_animation_player.play("PortalActive")
	else:
		_animation_player.play("PortalIdle")


func _use_portal(_actor: Actor) -> void:
#TODO instead of change the scene, hide the base scene. If we remove it,
#it won't continue to process materials. Let the GameManager handle this
	#get_tree().change_scene_to_file("res://src/maps/DungeonTest.tscn")
	print("teleport")


func _on_interactable_component_area_entered(_area: Area2D) -> void:
	if _interactable_component.is_enabled:
		change_state(PORTAL_STATE.active)


func _on_interactable_component_area_exited(_area: Area2D) -> void:
	change_state(PORTAL_STATE.idle)
