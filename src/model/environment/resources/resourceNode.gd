#TODO Show a progress indicator and a "click" interaction hint
#_interactable_component.interaction_callable = (func(): pass)

@tool
class_name ResourceNode extends StaticBody2D

signal clicked()

@export var required_tier: RItem.TOOL_TIERS
@export var required_tool: RItem.TOOL_TYPES

@export_range(0, 1, 1, "or_greater") var _resource_hp: int
@export var _destroy_on_zero_hp: bool
@export var _loot_progress_component: ComponentLootProgress

@export_category("Sprite")
@export var _sprite: Sprite2D
##The key resambles the resource hp and the value the sprite frame. If the
##resource hp has the value of the key, the sprite frame will be set to value.
@export var _resource_hp_to_sprite_map: Dictionary[int, int] = {}

@onready var _interactable_component: ComponentInteractable = $InteractableComponent

func _ready() -> void:
	if Engine.is_editor_hint():
		return

	_sprite.frame = _resource_hp_to_sprite_map.values()[_resource_hp_to_sprite_map.size() - 1]


func _on_sprite_clicked() -> void:
	clicked.emit()


func _update_sprite() -> void:
	if _resource_hp_to_sprite_map.has(_resource_hp):
		_sprite.frame = _resource_hp_to_sprite_map[_resource_hp]


func _update_resource_hp(value: int) -> void:
	_resource_hp = max(value, 0)

	_loot_progress_component.create_loot_items_by_step_value(_resource_hp)

	if _resource_hp <= 0.0:
		if _destroy_on_zero_hp:
			queue_free()
			return

		_interactable_component.toggle_enabled(false)

	_update_sprite()
