@tool
class_name ResourceNodeTree extends ResourceNode

func _on_tool_hurtbox_hurt(tool_damage: int) -> void:
#TODO communicate to the player, that his tool tier is to low
	if tool_damage < 0.0:
		return

	var base_damage := 1

	_update_resource_hp(
		_resource_hp - (tool_damage + base_damage) - required_tier
	)
