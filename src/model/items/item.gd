@tool
class_name Item extends Node2D

static var ITEM = preload("res://src/model/items/Item.tscn")

@export var data: RItem:
	set(value):
		data = value
		if value != null and is_node_ready():
			_setup_item()

@export var _currency_value: float
@export var _interaction_input: RInteractionInput

@export var _attractable_component: ComponentAttractable
@export var _auto_collectable_component: ComponentAutoCollectable
@export var _interactable_component: ComponentInteractable

@onready var _sprite_2d: Sprite2D = $Sprite2D

static func create(item_data: RItem, currency_value: float, pickup_delay: float = 0.0) -> Item:
	var item := ITEM.instantiate()

	item.data = item_data
	item.name = item_data.name
	item._currency_value = currency_value
	item._attractable_component.attraction_delay = pickup_delay

	return item


func _ready() -> void:
	if Engine.is_editor_hint():
		return

	_interactable_component.register_interaction(
		_interaction_input, _interaction_pick_up
	)

	_setup_item()


func _interaction_pick_up(_actor: Actor) -> void:
#TODO check inventory not full and other possible restrictions
	_attractable_component.is_enabled = true
	_auto_collectable_component.is_enabled = true


func get_currency_value() -> float:
	return _currency_value

func _setup_item() -> void:
	name = data.name

	_sprite_2d.texture = data.get_texture_for_value(_currency_value)

	if data.texture_scale_overwrite != 0.0:
		_sprite_2d.scale = Vector2(
			data.texture_scale_overwrite,
			data.texture_scale_overwrite
		)

	_attractable_component.is_enabled = data.is_attractable
	_auto_collectable_component.is_enabled = data.is_auto_collactable
	_interactable_component.is_enabled = data.is_interactable

	#_interactable_component.interaction_callable = _interact

	##Free hint when item is freed
	#tree_exiting.connect(_interactable_component.hint.queue_free)
