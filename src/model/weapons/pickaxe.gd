class_name Pickaxe extends Weapon

enum SWING_STATE{
	RIGHT_TO_LEFT,
	LEFT_TO_RIGHT,
}

@export var _next_swing: SWING_STATE = SWING_STATE.RIGHT_TO_LEFT

func attack() -> void:
	if _next_swing == SWING_STATE.LEFT_TO_RIGHT:
		_animation_player.play("pickaxe/swing_left_to_right")
		_next_swing = SWING_STATE.RIGHT_TO_LEFT
	elif _next_swing == SWING_STATE.RIGHT_TO_LEFT:
		_animation_player.play("pickaxe/swing_right_to_left")
		_next_swing = SWING_STATE.LEFT_TO_RIGHT
	else:
		_animation_player.play("RESET")
