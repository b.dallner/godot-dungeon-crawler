class_name Weapon extends Node2D

@export var is_attacking: bool

@export var _data: RItemWeapon
@export var _tool_component: ComponentTool

var possessor: Node2D

@warning_ignore("unused_private_class_variable")
@onready var _animation_player: AnimationPlayer = $AnimationPlayer

func attack() -> void:
	pass


func get_damage() -> float:
	return randf_range(_data.damage.x, _data.damage.y)


func get_tool_type() -> RItem.TOOL_TYPES:
	if _tool_component == null:
		return RItem.TOOL_TYPES.NONE

	return _tool_component.type


func get_tool_tier() -> RItem.TOOL_TIERS:
	if _tool_component == null:
		return RItem.TOOL_TIERS.ONE

	return _tool_component.tier
