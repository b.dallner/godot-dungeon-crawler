class_name REnemy extends RActor

enum DEATH_EFFECTS{
	NONE,
	SMOKE_PUFF
}

static var death_effects := {
	DEATH_EFFECTS.NONE: "",
	DEATH_EFFECTS.SMOKE_PUFF: "smoke_puff"
}

@export var name: String
@export_multiline var desc: String
@export_file("*.tscn") var scene: String
@export_range(0.0, 100.0, 1.0, "or_greater") var base_exp_reward: float

#region budget
@export_group("Budget")
@export_range(0.0, 100.0, 1.0, "or_greater") var budget_costs: int
#endregion

#region behavior
@export_group("Behavior")
@export var is_aggro: bool
@export var aggro_range: float
#endregion

#region visuals
@export_group("Visuals")
@export var texture: Texture2D
@export var hit_particals_texture: Texture2D
@export var death_effect: DEATH_EFFECTS
#endregion

#region sound
@export_group("Sound")
@export var death_sound_effect: AudioStream
#endregion
