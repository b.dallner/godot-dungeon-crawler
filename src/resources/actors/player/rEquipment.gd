class_name REquipment extends Resource

signal slot_changed(slot: RItem.SLOT_TYPES)

var slots: Dictionary[RItem.SLOT_TYPES, RItemEquipment] = {}

func _init() -> void:
	for key in RItem.SLOT_TYPES.values():
		slots[key] = null


func equip_or_swap_item(item: RItemEquipment) -> RItemEquipment:
	var prev_item = slots.get(item.slot_type)

	slots[item.slot_type] = item

	slot_changed.emit(item.slot_type)

	SoundBus.play_equipment_sound(item.equipping_sound)

	return prev_item


func unequip_item(item: RItemEquipment) -> void:
	slots[item.slot_type] = null

	SoundBus.play_equipment_sound(item.unequipping_sound)

	slot_changed.emit(item.slot_type)

#
#func replace_with(new_data: REquipment):
	#slots = new_data.slots
