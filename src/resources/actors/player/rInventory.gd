class_name RInventory extends Resource

@warning_ignore("unused_signal")
signal currency_changed()
@warning_ignore("unused_signal")
signal keys_changed()
@warning_ignore("unused_signal")
signal items_changed()

@export var currency: float
@export var keys: Array[RItemQuan]
@export var items: Array[RItemQuan]
