class_name RPlayer extends RActor

var player_pos: Vector2

@export var inventory: RInventory
@export var equipment: REquipment

#region level
@export var level: int
@export var required_exp: float
@export var current_exp: float
#endregion

func clone() -> RPlayer:
	var cloned_data := duplicate(true) as RPlayer

	cloned_data.inventory.keys.clear()
	cloned_data.inventory.items.clear()

	for key_quan in inventory.keys:
		cloned_data.inventory.keys.append(key_quan.duplicate())

	for item_quan in inventory.items:
		cloned_data.inventory.items.append(item_quan.duplicate())

	cloned_data.equipment.slots.clear()

	for slot in equipment.slots.keys():
		cloned_data.equipment.slots[slot] = equipment.slots[slot]

	return cloned_data
