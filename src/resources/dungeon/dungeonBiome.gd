class_name RDungeonBiome extends Resource

#TODO implement dungeon entrance rooms
@export var room_entrance: PackedScene
@export var rooms: Array[PackedScene]
@export var room_exit: PackedScene

@export var enemies: Array[REnemy]
@export var resource_nodes: Array[PackedScene]
