class_name RDungeonRoomParameter extends Resource

enum DIFFICULTIES{
	EASY,
	MEDIUM,
	HARD,
}

@export_group("Difficulty")
##Difficulty
@export var difficulty: DIFFICULTIES
##Increase of available budget over time
@export var scaling: Curve
##Time between ticks in seconds
@export_range(0.0, 5.0, 0.1, "or_greater","suffix:sec") var tick_speed: float
##
@export_range(0, 10, 1, "or_greater") var max_tick_progress_without_actions: int
##Resources available for spawning monster
@export var available_budget: int
##Base budget available for ticks. Scaling is applied to increas the value
##base * (1 + scaling * tick_resources_scaling_multiplyer)
@export_range(0, 100, 10, "or_greater") var tick_budget: int
##Modifies how much the tick budget are effected by the scaling curve
@export_range(1.0, 10.0, 0.1, "or_greater") var tick_budget_scaling_multiplier: float
##Maximum number of monster able to spawn at the same time. Scaling is applied
##to increase the value base * (1 + scaling * num_enemies_scaling_multiplyer)
@export_range(0, 10, 1, "or_greater") var max_num_enemies: int
##Modifies how much the max number of enemies is effected by the scaling curve
@export_range(1.0, 10.0, 0.1, "or_greater") var num_enemies_scaling_multiplier: float
