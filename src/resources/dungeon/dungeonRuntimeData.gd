class_name RDungeonRuntimeData extends Resource

@export var rng_seed: int
@export var rng_state: int
@export var biome: RDungeonBiome
@export var parameter: RDungeonRoomParameter
@export var timer: float

var active_room_runtime_data := RDungeonRoomRuntimeData.new()

#TODO need to track all kinds of data eg. loot, kills, exp, gold
#ALl the things we need to recreate an identical dungeon for re-runs
