@tool
class_name RItemEquipment extends RItem

@export var model_scene_path: String
@export var equipping_sound: AudioStream
@export var unequipping_sound: AudioStream

@export var slot_type: SLOT_TYPES
