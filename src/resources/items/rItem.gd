@tool #only a tool becaus of other tool scripts dependencies
class_name RItem extends Resource

enum TOOL_TIERS{
	ONE,
	TWO,
	THREE,
}

enum TOOL_TYPES{
	NONE,
	AXE,
	PICKAXE,
}

enum SLOT_TYPES{
	NONE,
	WEAPON,
	ARMOR_HEAD,
	ARMOR_CHEST,
	ARMOR_FEET,
	ARMOR_HAND,
	TOOL_AXE,
	TOOL_PICKAXE,
}

@export var name: String
@export_multiline var desc: String
@export var texture: Texture2D
@export_range(0.0, 1.0, 0.01, "or_greater") var texture_scale_overwrite: float

#TBD should this be in the RLoot resource?
@export_group("Player interaction")
@export var is_auto_collactable: bool
@export var is_attractable: bool
@export var is_interactable: bool

@export_group("Quantity")
@export var is_stackable: bool
@export var max_stack_size: int

@export_group("Sound")
@export var pickup_sound: AudioStream


##Overwrite function
func get_texture_for_value(_value: float) -> Texture2D:
	return texture
