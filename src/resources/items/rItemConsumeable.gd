@tool
class_name RItemConsumeable extends RItem

@export var stats_effects: Array[RStatsEffect] = []
@export var stats_buffs: Array[RStatsBuff] = []

@export_group("Sound")
@export var consumtion_sound: AudioStream
