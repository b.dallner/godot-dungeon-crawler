@tool
class_name RItemCurrency extends RItem

const ICON_SIZE := Vector2(32, 32)

@export var _value_mapping: Array[Vector3i]

#func init() -> void:
	#is_currency = true


func get_texture_for_value(value: float) -> Texture2D:
	var icon_pos: Vector2i
	var atlas: AtlasTexture = texture.duplicate()

	for vm in _value_mapping:
		if vm.z >= value:
			icon_pos = Vector2i(vm.x, vm.y)
			break

	atlas.region = Rect2(icon_pos, ICON_SIZE)

	return atlas
