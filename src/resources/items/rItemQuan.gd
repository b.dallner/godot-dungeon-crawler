class_name RItemQuan extends Resource

@export var item: RItem
@export var quantity: float = 1.0

@warning_ignore("shadowed_variable")
static func create_instance(item: RItem, quantity: float = 1.0) -> RItemQuan:
	var instance = RItemQuan.new()

	instance.item = item
	instance.quantity = quantity

	return instance
