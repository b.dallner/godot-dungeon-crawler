@tool
class_name RItemWeapon extends RItemEquipment

@export var damage: Vector2
@export var attack_speed: float
