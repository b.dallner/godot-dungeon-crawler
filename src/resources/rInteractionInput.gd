class_name RInteractionInput extends Resource

@warning_ignore("unused_signal")
signal triggered(actor: Actor)

@export var input: InputMapping.INTERACTION_INPUTS
@export var interaction_name: String
@export_range(1, 10, 1, "or_greater") var ui_list_position: int

var _physical_keycode: Key

func get_physical_keycode() -> Key:
	_physical_keycode = InputMapping.get_physical_keycode(
		InputMapping.get_action_name(input)
	)

	return _physical_keycode
