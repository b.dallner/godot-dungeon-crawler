class_name RLoot extends Resource

@export var item: RItem
@export var currency_value_range: Vector2i
@export var quantity_range: Vector2i

@export_range(0.0, 1.0, 0.05, "or_greater", "suffix:sec")
var pickup_delay_sec: float = 0.3
