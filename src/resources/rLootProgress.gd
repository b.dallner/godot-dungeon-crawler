@tool
class_name RLootProgress extends Resource

@export var value: float

@export var guaranteed_loot: Array[RLoot] = []
@export var rng_loot: Array[RRngLoot] = []
