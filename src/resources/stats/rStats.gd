@tool
class_name RStats extends Resource

signal max_hp_changed()

@export_range(0.0, 250.0, 1.0, "or_greater") var max_hp: float:
	set(value):
		max_hp = value
		max_hp_changed.emit()
		changed.emit()

@export_range(0.0, 2000.0, 100.0, "or_greater") var walking_speed: float:
	set(value):
		walking_speed = value
		changed.emit()

@export_range(0.0, 4000.0, 100.0, "or_greater") var sprinting_speed: float:
	set(value):
		sprinting_speed = value
		changed.emit()

@export_category("Enemy specific")
@export_range(0.0, 20.0, 1.0, "or_greater") var attack_range: float
@export_range(0.0, 3.0, 0.1, "or_greater") var attack_cooldown: float
