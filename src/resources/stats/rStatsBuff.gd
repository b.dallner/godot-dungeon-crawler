@tool
class_name RStatsBuff extends Resource

@warning_ignore("unused_signal")
signal buff_reapplied()

@export var name: String
@export_multiline var desc: String
@export var icon: Texture2D
@export var effected_stats: RRuntimeStats = RRuntimeStats.new()
@export var is_percantage: bool
@export var priority: int
@export var remove_on_player_death: bool = true

@export_group("Duration")
##Duration of the buff. -1 means no duration
@export_range(0, 60, 1, "or_greater", "suffix:sec") var duration_sec: float
@export var is_spread_over_time: bool

##Time between the effect being applied, if spread over time
@export_range(0.0, 10.0, 0.5, "or_greater", "suffix:sec") var tick_speed_sec: float:
	set(value):
		tick_speed_sec = value

		if is_spread_over_time:
			if spread_curve == null:
				spread_curve = Curve.new()

			_update_curve()

##Controlls the percentage of the stats value applied each tick.
##The number of ticks should be equal to the bake resolution
@export var spread_curve: Curve

#region tooling
@export_group("Duration/Tooling")
@export_tool_button("Bake curve") var bake_curve: Callable = func():
	if spread_curve != null:
		spread_curve.bake()

@export_tool_button("Update curve sum") var update_sum_curve_values: Callable = func():
	sum_curve_values = _calc_sum_curve_value()

@export var update_curve_when_tick_time_changes: bool
@export var sum_curve_values: float
#endregion

#var remaining_duration: float

func _update_curve() -> void:
	var ticks: int

	if duration_sec == -1:
		ticks = 1
	else:
		ticks = ceili(duration_sec / tick_speed_sec)

	var average_percentage := 100.0 / ticks

	spread_curve.clear_points()

	spread_curve.max_value = average_percentage * 2
	spread_curve.bake_resolution = ticks
	spread_curve.add_point(Vector2(0.0, average_percentage))
	spread_curve.add_point(Vector2(ticks, average_percentage))

	spread_curve.bake()


#region tooling
func _calc_sum_curve_value() -> float:
	if spread_curve == null:
		return 0.0

	var sum := 0.0

	for i in spread_curve.bake_resolution:
		sum += spread_curve.sample_baked(
			1.0 / spread_curve.bake_resolution * i
		)

	return sum
#endregion
