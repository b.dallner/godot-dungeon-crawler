@tool
class_name RStatsEffect extends Resource

@export var effected_stats: RRuntimeStats = RRuntimeStats.new()
@export var priority: int

@export var is_percentage: bool
