@tool
class_name RRuntimeStats extends RStats

signal current_hp_changed()
signal invisiblility_changed()

#@export var stats: RStats = RStats.new()
@export var is_invincible: bool
@export var is_invisible: bool:
	set(value):
		is_invisible = value
		invisiblility_changed.emit()

@export var current_hp: float:
	set(value):
		current_hp = value
		current_hp_changed.emit()

func validate_current_hp() -> void:
	current_hp = clampf(current_hp, 0.0, max_hp)
