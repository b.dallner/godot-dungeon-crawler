class_name UiInteractionButton extends Button

const INTERACTION_BUTTON = preload("res://src/ui/interactions/InteractionButton.tscn")

var _interaction_input: RInteractionInput
var _player: Player

static func create(interaction_input: RInteractionInput, player: Player) -> UiInteractionButton:
	var button := INTERACTION_BUTTON.instantiate()

	button._interaction_input = interaction_input
	button._player = player

	return button


func _ready() -> void:
	assert(_interaction_input != null, "No interaction Input set for UiInteractionButton")

	text = "[%s] %s" % [
		InputMapping.get_key_for_interaction_input(_interaction_input.input),
		_interaction_input.interaction_name,
	]

	var input_event_key := InputEventKey.new()
	input_event_key.physical_keycode = _interaction_input.get_physical_keycode()


func _on_pressed() -> void:
	_interaction_input.triggered.emit(_player)
