class_name InteractionHint extends Control

@export var color_active: Color

var action: String
var is_active: bool:
	set(value):
		is_active = value
		if value:
			modulate = color_active
		else:
			modulate = Color.WHITE

var interactable: Node2D

@onready var _button: Label = %Button

func _ready() -> void:
#TODO test if its updated when the key is rebinded
	_button.text = "[" + InputMapping.get_input_for_action(action) + "]"


func _physics_process(_delta: float) -> void:
	if interactable != null:
		global_position = interactable.get_global_transform_with_canvas().origin
