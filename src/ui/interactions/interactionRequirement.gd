class_name UiInteractionRequirement extends Control

const VERTICAL_OFFSET = -24
const INTERACTION_REQUIREMENT = preload("res://src/ui/interactions/InteractionRequirement.tscn")

var _requirements: Array[RItemRequirement]
var _interactable: Node2D

var _item_requirements: Array[UiItemRequirement]

@onready var _items: HBoxContainer = $CenterContainer/ItemRequirements

static func create(requirements: Array[RItemRequirement], interactable: Node2D) -> UiInteractionRequirement:
	var node = INTERACTION_REQUIREMENT.instantiate()

	node._requirements = requirements
	node._interactable = interactable

	return node


func _ready() -> void:
	global_position = (
		( _interactable.get_global_transform_with_canvas().origin +
		Vector2(0, VERTICAL_OFFSET) )
	)

	for requirement in _requirements:
		var item_requirement_scenes = _create_item_requirement_scenes(
			requirement.item_quan
		)

		_item_requirements.append_array(item_requirement_scenes)

	for r in _item_requirements:
		_items.add_child(r)


func _create_item_requirement_scenes(item_qaun: RItemQuan) -> Array[UiItemRequirement]:
	var nodes: Array[UiItemRequirement] = []

	if item_qaun.item is RItemKey:
		for i in item_qaun.quantity:
			var node := UiItemRequirement.create(item_qaun)

			nodes.append(node)
	else:
		var node := UiItemRequirement.create(item_qaun)

		nodes.append(node)

	return nodes


func show_available_items(inventory: RInventory) -> void:
	_show_available_currency(inventory.currency)
	_show_available_keys(inventory.keys)
	_show_available_items(inventory.items)


func reset_visuals() -> void:
	for item_requirement in _item_requirements:
		item_requirement.modulate = Color(1,1,1,0.5)
		item_requirement.toggle_float_animation(false)


func _show_available_currency(currency: float) -> void:
	for item_requirement in _item_requirements:
		if item_requirement.required_item_quan.item is not RItemCurrency:
			continue

		if item_requirement.required_item_quan.quantity > currency:
			break

		item_requirement.modulate = Color.WHITE
		item_requirement.toggle_float_animation(true)
		currency -= item_requirement.required_item_quan.quantity


func _show_available_keys(keys: Array[RItemQuan]) -> void:
	for item_qaun in keys:
		var item_quantity = item_qaun.quantity

		for item_requirement in _item_requirements:
			if item_requirement.required_item_quan.item is not RItemKey:
				continue

			if item_requirement.required_item_quan.item != item_qaun.item or item_quantity == 0:
				continue

			item_requirement.modulate = Color.WHITE
			item_requirement.toggle_float_animation(true)
			item_quantity -= 1

			if item_quantity == 0:
				break


func _show_available_items(items: Array[RItemQuan]) -> void:
#TBD close to identical to show available keys
	for item_qaun in items:
		var item_quantity = item_qaun.quantity

		for item_requirement in _item_requirements:
			if item_requirement.required_item_quan.item != item_qaun.item:
				continue

			item_requirement.modulate = Color.WHITE
			item_requirement.toggle_float_animation(true)
			item_quantity -= 1

			if item_quantity == 0:
				break
