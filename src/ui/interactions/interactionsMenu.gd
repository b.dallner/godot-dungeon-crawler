class_name UiInteractionsMenu extends ScrollContainer

var player: Player

@onready var _interaction_buttons: VBoxContainer = %InteractionButtons
@onready var _interactable_name: Label = $PanelContainer/MarginContainer/VBoxContainer/InteractableName

func set_interactable(interactable: ComponentInteractable) -> void:
	var interaction_inputs := interactable.get_interaction_inputs()
	_interactable_name.text = interactable.interactable_owner.name

	interaction_inputs.sort_custom(
		func(a: RInteractionInput, b: RInteractionInput):
			return a.ui_list_position < b.ui_list_position
	)

	for interaction_input: RInteractionInput in interaction_inputs:
		_interaction_buttons.add_child(
			UiInteractionButton.create(interaction_input, player)
		)


func clear() -> void:
	for c in _interaction_buttons.get_children():
		_interaction_buttons.remove_child(c)
