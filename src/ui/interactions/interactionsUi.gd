class_name UiInteractionRequirements extends CanvasLayer

var _focus_manager: FocusManger
var _player_data: RPlayer

func _ready() -> void:
	SignalBus.ui_item_requirements_requested.connect(
		_on_ui_item_requirements_requested
	)
	SignalBus.ui_item_requirements_removed.connect(
		_on_ui_item_requirements_removed
	)


func _on_ui_item_requirements_requested(requirements: Array[RItemRequirement], interactable_component: ComponentInteractable) -> void:
	var node := UiInteractionRequirement.create(
		requirements, interactable_component.interactable_owner
	)

	node.name = interactable_component.interactable_owner.name

	interactable_component.focus_changed.connect(func(has_focus: bool):
		if has_focus:
			node.show_available_items(_player_data.inventory)
		else:
			node.reset_visuals()
	)

	add_child(node)

#TBD tmp solution for messed up scalings
	node.global_position = node.global_position / scale


func _on_ui_item_requirements_removed(interactable_component: ComponentInteractable) -> void:
	var node := get_node_or_null(
		str(interactable_component.interactable_owner.name)
	)

	if node == null:
		return

	node.queue_free()


func _on_player_inventory_changed() -> void:
	if _focus_manager.focused_interactable == null:
		return

	var node: UiInteractionRequirement = get_node_or_null(
		str(_focus_manager.focused_interactable.interactable_owner.name)
	)

	if node == null:
		return

	node.show_available_items(_player_data.inventory)


func setup(player_data: RPlayer, focus_manager: FocusManger) -> void:
	_focus_manager = focus_manager

	#Setup can be could with the same player data multiple times. This happens
	#when the player dies without reaching a checkpoint
	if _player_data != player_data:
		player_data.inventory.currency_changed.connect(_on_player_inventory_changed)
		player_data.inventory.keys_changed.connect(_on_player_inventory_changed)
		player_data.inventory.items_changed.connect(_on_player_inventory_changed)

	_player_data = player_data
