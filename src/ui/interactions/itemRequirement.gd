class_name UiItemRequirement extends HBoxContainer

const ITEM_REQUIREMENT = preload("res://src/ui/interactions/ItemRequirement.tscn")

var required_item_quan: RItemQuan

@onready var _animation_player: AnimationPlayer = $AnimationPlayer
@onready var _label: Label = $Label
@onready var _texture_rect: TextureRect = $TextureRect

static func create(item_quan: RItemQuan) -> UiItemRequirement:
	var node = ITEM_REQUIREMENT.instantiate()

	node.required_item_quan = item_quan

	return node


func _ready() -> void:
	_texture_rect.texture = required_item_quan.item.texture

	if required_item_quan.quantity > 1 and required_item_quan.item is not RItemKey:
		_label.text = "%.f" % required_item_quan.quantity
	else:
		_label.visible = false

	modulate = Color(1,1,1,0.5)


func toggle_float_animation(value: bool) -> void:
	if value:
		_animation_player.play("floating")
	else:
		_animation_player.play("RESET")
