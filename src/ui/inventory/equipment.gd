class_name UiEquipment extends PanelContainer

var equipment: REquipment:
	set(value):
		equipment = value
		equipment.slot_changed.connect(_on_slot_changed)

		_update_slots()


var _slots: Array[Control]

@onready var _equipment_slots: Control = %EquipmentSlots
@onready var _tool_slots: HBoxContainer = %ToolSlots

func _ready() -> void:
	_slots.append_array(_equipment_slots.get_children())
	_slots.append_array(_tool_slots.get_children())


func _on_slot_changed(slot: RItem.SLOT_TYPES) -> void:
	for cell:EquipmentCell in _slots:
		if cell.slot_type != slot:
			continue

		cell.item = equipment.slots[slot]
		return


func _update_slots() -> void:
	for cell:EquipmentCell in _slots:
		cell.item = null

	for item:RItemEquipment in equipment.slots.values():
		if item == null:
			continue

		_on_slot_changed(item.slot_type)
