@tool
class_name EquipmentCell extends Control

@export var slot_type: RItem.SLOT_TYPES

@export var _placeholder_icon: Texture2D:
	set(value):
		_placeholder_icon = value

		if is_inside_tree():
			_placeholder.texture = value

@onready var _placeholder: TextureRect = $Placeholder
@onready var _item_texture: TextureRect = %ItemTexture

var item: RItem:
	set(value):
		item = value

		if item == null:
			_placeholder.show()
			_item_texture.texture = null
		else:
			_placeholder.hide()
			_item_texture.texture = item.texture


func _ready() -> void:
	_placeholder.texture = _placeholder_icon


func _on_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if item == null:
			return

		if event.is_action_pressed("equip_item"):
			SignalBus.ui_inventory_item_unequipped.emit(item)
