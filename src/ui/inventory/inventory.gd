class_name UiInventory extends PanelContainer

const INVENTORY_CELL = preload("res://src/ui/inventory/InventoryCell.tscn")

var inventory: RInventory:
	set(value):
		inventory = value
		inventory.items_changed.connect(update_cells)

		update_cells()

var _cells: Array[UiInventoryCell]

@export var _grid_cells: int

@onready var _item_grid: GridContainer = %ItemGrid

func _ready() -> void:
	for i in _grid_cells:
		var cell := INVENTORY_CELL.instantiate()

		_cells.append(cell)
		_item_grid.add_child(cell)


func update_cells() -> void:
	for cell in _cells:
		cell.item_qaun = null

	for item_qaun in inventory.items:
		for cell in _cells:
			if cell.item_qaun != null:
				continue

			cell.tooltip_text = item_qaun.item.name
			cell.item_qaun = item_qaun
			break
