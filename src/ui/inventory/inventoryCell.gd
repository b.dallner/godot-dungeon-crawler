class_name UiInventoryCell extends Control

@export var _highlight_color: Color

@onready var _background: TextureRect = $Background
@onready var _item_texture: TextureRect = %ItemTexture

var item_qaun: RItemQuan:
	set(value):
		item_qaun = value
		if value == null:
			_item_texture.texture = null
		else:
			_item_texture.texture = value.item.texture
			_item_texture.tooltip_text = item_qaun.item.name

var _selected: bool

func _on_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if item_qaun == null:
			return

		if event.is_action_pressed("left_click"):
			_selected = !_selected

			if _selected:
				_background.modulate = _highlight_color
			else:
				_background.modulate = Color.WHITE
		elif event.is_action_pressed("use_item"):
			#TBD double click has a long duration, should be shorter
			#if event.double_click:
			if item_qaun.item is RItemConsumeable:
				SignalBus.ui_inventory_item_consumed.emit(item_qaun)
			elif item_qaun.item is RItemEquipment:
				SignalBus.ui_inventory_item_equipped.emit(item_qaun.item)
