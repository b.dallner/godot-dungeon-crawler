class_name UiKeyCounter extends HBoxContainer

var num_keys: int:
	set(value):
		num_keys = value
		_num_keys.text = str(value)

var key: RItem:
	set(value):
		key = value
		_texture_rect.texture = value.texture

@onready var _num_keys: Label = $Label
@onready var _texture_rect: TextureRect = $TextureRect
