class_name UiKeys extends PanelContainer

const KEY_COUNTER = preload("res://src/ui/KeyCounter.tscn")

@onready var v_box_keys: VBoxContainer = $MarginContainer/VBoxKeys

var player_data: RPlayer:
	set(value):
		player_data = value
		player_data.inventory.keys_changed.connect(_on_keys_changed)

		_clear_children()

		await get_tree().physics_frame

		_on_keys_changed()


func _on_keys_changed() -> void:
	for item_quan: RItemQuan in player_data.inventory.keys:
		var key_counters = v_box_keys.get_children().filter(
			func(kc:UiKeyCounter): return kc.key == item_quan.item
		)

		var key_counter: UiKeyCounter

		if key_counters.size() == 0:
			key_counter = _create_key_counter(item_quan)

			v_box_keys.add_child(key_counter)

			key_counters.append(key_counter)
		else:
			key_counter = key_counters[0]
			key_counter.num_keys = int(item_quan.quantity)

	if v_box_keys.get_child_count() > 0:
		visible = true


func _create_key_counter(item_quan: RItemQuan) -> UiKeyCounter:
	var key_counter := KEY_COUNTER.instantiate() as UiKeyCounter

	key_counter.ready.connect(func():
		key_counter.key = item_quan.item
		key_counter.num_keys = int(item_quan.quantity)
	)

	return key_counter


func _clear_children() -> void:
	visible = false

	for c in v_box_keys.get_children():
		c.queue_free()
