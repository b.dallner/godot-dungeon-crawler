class_name UiPlayer extends CanvasLayer

var _focus_manager: FocusManger
var _player: Player
#var _player_data: RPlayer

@onready var _exp_bar: TextureProgressBar = %Exp
@onready var _hp_bar: TextureProgressBar = %Hp
@onready var _currency: Label = %Currency

@onready var _keys: UiKeys = %PanelContainerKeys
@onready var _player_buffs: HBoxContainer = %HBoxPlayerBuffs
@onready var _player_equipment: UiEquipment = %Equipment
@onready var _player_inventory: UiInventory = %Inventory

@onready var _interactions_menu: UiInteractionsMenu = %InteractionsMenu

@onready var _dungeon_timers: GridContainer = %GridDungeonTimers
@onready var _dungeon_timer: Label = %DungeonTimer
@onready var _room_timer: Label = %RoomTimer

var _visible_uis: Array[Control]
var _dungeon_runtime_data: RDungeonRuntimeData

func _ready() -> void:
	SignalBus.ui_add_element_requested.connect(_add_ui_element)
	SignalBus.ui_remove_element_requested.connect(_remove_ui_element)

	SignalBus.ui_add_buff_requested.connect(_on_add_buff_requested)

	SignalBus.ui_inventory_item_equipped.connect(func(_item: RItem):
		if _player_equipment.visible == false:
			_player_equipment.show()
			_handle_ui_stack(_player_equipment)
	)

	SignalBus.ui_interaction_menu_show_requested.connect(
		func(i: ComponentInteractable):
			_interactions_menu.clear()
			_interactions_menu.set_interactable(i)
			_interactions_menu.show()
	)

	SignalBus.ui_interaction_menu_hide_requested.connect(func():
		_interactions_menu.hide()
		_interactions_menu.clear()
	)

	SignalBus.dungeon_started.connect(_on_dungeon_started)


func _physics_process(_delta: float) -> void:
	if _dungeon_runtime_data == null:
		return

	_dungeon_timer.text = _create_timer_string(_dungeon_runtime_data.timer)
	_room_timer.text = _create_timer_string(
		_dungeon_runtime_data.active_room_runtime_data.timer
	)


func _unhandled_key_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		if _visible_uis.size() > 0:
			var element := _visible_uis.pop_back() as Control
			element.visible = false
		else:
			get_tree().quit()

	if event.is_action_pressed("inventory"):
		_player_inventory.visible = !_player_inventory.visible
		_handle_ui_stack(_player_inventory)

	elif event.is_action_pressed("equipment"):
		_player_equipment.visible = !_player_equipment.visible
		_handle_ui_stack(_player_equipment)

	elif event.is_action_pressed("ui_focus_next"):
		if event.shift_pressed:
			_focus_manager.focus_prev()
		else:
			_focus_manager.focus_next()


func _on_currency_changed() -> void:
	_currency.text = str(_player.data.inventory.currency)


func _on_max_hp_updated() -> void:
	%MaxHp.text = str(_player.runtime_stats.max_hp)
	_hp_bar.max_value = _player.runtime_stats.max_hp


func _on_current_hp_updated() -> void:
	%CurrentHp.text = str(_player.runtime_stats.current_hp)
	_hp_bar.value = _player.runtime_stats.current_hp


func _on_required_exp_updated() -> void:
	_exp_bar.max_value = _player.data.required_exp


func _on_exp_updated() -> void:
	_exp_bar.value = _player.data.current_exp


func _on_dungeon_started(dungeon_runtime_data: RDungeonRuntimeData) -> void:
	_dungeon_runtime_data = dungeon_runtime_data
	_dungeon_timers.visible = true


func _on_add_buff_requested(buff: Buff) -> void:
	var ui_buff = UiBuff.create(buff)

	buff.buff_timeout.connect(func(_buff: Buff):
		ui_buff.set_process(false)
		ui_buff.queue_free()
	)

	_player_buffs.add_child(ui_buff)


func setup(player: Player) -> void:
	_focus_manager = player.focus_manager
	_player_inventory.inventory = player.data.inventory
	_player_equipment.equipment = player.data.equipment
	_interactions_menu.player = player

	_exp_bar.max_value = player.data.required_exp
	_exp_bar.value = player.data.current_exp

	_currency.text = str(player.data.inventory.currency)

	#Setup can be could with the same player data multiple times. This happens
	#when the player dies without reaching a checkpoint
	#if _player_data != player.data:
	if _player != player:
		_keys.player_data = player.data

		player.data.inventory.currency_changed.connect(_on_currency_changed)
		player.runtime_stats.max_hp_changed.connect(_on_max_hp_updated)
		player.runtime_stats.current_hp_changed.connect(_on_current_hp_updated)

	#_player_data = player.data
	_player = player

	_on_max_hp_updated()
	_on_current_hp_updated()


func _handle_ui_stack(element: Control) -> void:
	if element.visible:
		_visible_uis.append(element)
	else:
		_visible_uis.remove_at(_visible_uis.find(element))


func _add_ui_element(element: Control) -> void:
	add_child(element)


func _remove_ui_element(element: Control) -> void:
	remove_child(element)


func _create_timer_string(timer: float) -> String:
	var minutes := int(timer / 60)

	return "%d:%.1f" % [minutes, fmod(timer, 60.0)]
