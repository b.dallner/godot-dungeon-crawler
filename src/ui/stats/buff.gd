class_name UiBuff extends TextureRect

const UI_BUFF = preload("res://src/ui/stats/Buff.tscn")

var _buff: Buff

@export var _tooltip_offset: Vector2 = Vector2i(0, 16)

@onready var _remaining_duration: PanelContainer = $RemainingDuration
@onready var _duration: Label = %Duration
@onready var _skill: Label = %Skill
@onready var _progress_remaining_duration: TextureProgressBar = $ProgressRemainingDuration

static func create(buff: Buff) -> UiBuff:
	var ui_buff = UI_BUFF.instantiate()

	ui_buff._buff = buff

	return ui_buff


func _ready() -> void:
	texture = _buff.stats_buff.icon

	_skill.text = _buff.stats_buff.name
	_progress_remaining_duration.max_value = _buff.stats_buff.duration_sec

	if _buff.stats_buff.duration_sec == -1:
		_duration.hide()
		_progress_remaining_duration.hide()

	##Check if mouse position is over the element
	if Rect2(Vector2(), size).has_point(get_local_mouse_position()):
		_remaining_duration.show()


func _physics_process(_delta: float) -> void:
	if _remaining_duration.visible:
		var minutes := int(_buff.get_remaining_lifetime() / 60)

		if minutes > 0:
			_duration.text = "%d:%d min" % [
				minutes, fmod(_buff.get_remaining_lifetime(), 60.0)
			]
		else:
			_duration.text = "%.1f sec" % [_buff.get_remaining_lifetime()]

		_remaining_duration.position = get_viewport().get_mouse_position() + _tooltip_offset

	_progress_remaining_duration.value = _buff.stats_buff.duration_sec -\
		_buff.get_remaining_lifetime()


func _on_mouse_entered() -> void:
	_remaining_duration.show()


func _on_mouse_exited() -> void:
	_remaining_duration.hide()
